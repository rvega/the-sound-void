SV_CFLAGS = "-std=c99 -Wall -Wextra -Wuninitialized -fdiagnostics-color=always"

SV_LINK_FLAGS= "-lm"

SV_CFLAGS_DEBUG = "-pedantic -Og -g -DDEBUG -fsanitize=address -fsanitize-recover=address -fsanitize=undefined -fno-omit-frame-pointer"

SV_CFLAGS_RELEASE = "-O3 -DNDEBUG"

def sv_get_cflags(arguments):
    is_release = int(arguments.get('release', 0))
    if(is_release):
        return SV_CFLAGS.split() + SV_CFLAGS_RELEASE.split()
    else:
        return SV_CFLAGS.split() + SV_CFLAGS_DEBUG.split()

def sv_get_linkflags(arguments):
    return SV_LINK_FLAGS.split()
