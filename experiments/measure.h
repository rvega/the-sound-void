#ifndef SV_MEASURE_H
#define SV_MEASURE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <soundvoid/basics.h>
#include <soundvoid/time.h>
#include <stdlib.h>

struct sv_measure_experiment {
  uint iterations_per_sample;
  uint max_measurements;
  uint num_measurements;
  uint max_samples;
  uint num_samples;
};
typedef struct sv_measure_experiment sv_measure_experiment;

void sv_measure_experiment_init(sv_measure_experiment *e, char *name,
                                uint iterations_per_sample,
                                uint samples_per_measurement,
                                uint max_measurements);

extern void sv_measure_add_measurement(sv_measure_experiment *e, float x);

extern void sv_measure_add_sample(sv_measure_experiment *e, float sample);

extern void sv_measure_experiment_free(sv_measure_experiment *e);

extern void sv_measure_print(char *name, sv_measure_experiment *e);

#define sv_measure_iterate_start(name, iterations, i, samples, s,              \
                                 measurements, m)                              \
  if (1) {                                                                     \
    sv_timespec t1 = {0};                                                      \
    sv_timespec t2 = {0};                                                      \
    sv_measure_experiment e = {0};                                             \
    sv_measure_experiment_init(&e, name, iterations, samples, measurements);   \
    for (m = 0; m < e.max_measurements; ++m) {                                 \
      for (s = 0; s < e.max_samples; ++s) {                                    \
        sv_timespec_get(&t1);                                                  \
        for (i = 0; i < e.iterations_per_sample; ++i) {

#define sv_measure_iterate_end(x)                                              \
  } /* end for i */                                                            \
  sv_timespec_get(&t2);                                                        \
  float duration = sv_timespec_diff(t1, t2) / e.iterations_per_sample;         \
  sv_measure_add_sample(&e, duration);                                         \
  } /* end for s */                                                            \
  sv_measure_add_measurement(&e, x);                                           \
  } /* end for m */                                                            \
  } /* end if */

#endif /* ifndef SV_MEASURE */

#ifdef __cplusplus
}
#endif
