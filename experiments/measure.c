#include "measure.h"
#include <soundvoid/print.h>

void sv_measure_experiment_init(sv_measure_experiment* e, char* name,
                                uint iterations_per_sample,
                                uint samples_per_measurement,
                                uint max_measurements) {
  e->iterations_per_sample = iterations_per_sample;
  e->max_measurements = max_measurements;
  e->max_samples = samples_per_measurement;
  e->num_measurements = 0;
  e->num_samples = 0;

  sv_print_str("\n\n{\"name\":\"");
  sv_print_str(name);
  sv_print_str("\", \"data\":[\n");

  /* Start first measurement */
  sv_print_str("{\"t\":[");
}

void sv_measure_add_sample(sv_measure_experiment* e, float sample) {
  if (e->num_samples >= e->max_samples) {
    return;
  }

  /* Print the sample */
  sv_print_float(sample, 10);
  e->num_samples++;

  /* If this is not the last sample, print a comma */
  if (e->num_samples < e->max_samples) {
    sv_print_str(", ");
  }
}

extern void sv_measure_add_measurement(sv_measure_experiment* e, float x) {
  if (e->num_measurements >= e->max_measurements) {
    return;
  }

  e->num_samples = 0;
  e->num_measurements++;

  /* End samples array, end previous measurement with x value.*/
  sv_print_str("], \"x\":");
  sv_print_float(x, 10);
  sv_print_str("}");

  /* if not the last one */
  if (e->num_measurements < e->max_measurements) {
    /* Start new measurement */
    sv_print_str(",\n{\"t\":[");
  }
  else {
    // End data object
    sv_print_str("\n]}");
  }
}
