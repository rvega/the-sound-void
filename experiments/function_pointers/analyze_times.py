import json
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np


def bar_plot(axis, data, color, label):
    kwargs = dict(ecolor=color,
                  color=color,
                  capsize=2,
                  elinewidth=1.1,
                  linewidth=0.6)
    x = []
    y = []
    err = []
    accum = []
    for measurement in data:
        x.append(measurement['x'])

        accum += measurement['t']
        median = np.median(measurement['t'])
        # mean = np.mean(measurement['t'])
        # y.append(mean)

        y.append(median)
        variance = np.std(measurement['t'], ddof=1)
        err.append(variance)

    # accum_median = np.mean(accum)
    accum_median = np.median(accum)
    axis.errorbar(x,
                  y,
                  yerr=err,
                  fmt='-o',
                  mfc=color,
                  **kwargs,
                  label=label + ' ' +
                  np.format_float_scientific(accum_median, 3))


def analyze(title, data_file):
    f = open(data_file, 'r')
    data = json.loads(f.read())
    f.close()

    plt.rcParams.update({'font.size': 8})
    figure, axes = plt.subplots(3,
                                1,
                                constrained_layout=True,
                                figsize=(10.24, 7.68))
    figure.suptitle(title)

    # 1. Simple comparison between function pointers and regular function call.
    data_11 = np.array(data[0]['data'])
    bar_plot(axes[0], data_11, 'r', 'Call do_tic directly')

    data_12 = np.array(data[1]['data'])
    bar_plot(axes[0], data_12, 'b', 'Pointers')

    # 2. Callbacks that do nothing
    data_22 = np.array(data[3]['data'])
    bar_plot(axes[1], data_22, 'b', 'Pointer to do_nothing')

    data_23 = np.array(data[4]['data'])
    bar_plot(axes[1], data_23, 'g', 'Call do_nothing directly')

    data_21 = np.array(data[2]['data'])
    bar_plot(axes[1], data_21, 'r', 'Pointer to null')

    # 3. Callbacks that do some work
    data_31 = np.array(data[5]['data'])
    bar_plot(axes[2], data_31, 'r', 'Pointer to do_tic')

    data_32 = np.array(data[6]['data'])
    bar_plot(axes[2], data_32, 'g', 'Call do_tic directly')

    axes[0].set_title('Pointer vs direct calls. No checks')
    axes[0].legend(loc='best', frameon=True)
    axes[0].set_ylabel('Seconds')
    axes[0].get_yaxis().set_major_formatter(mtick.FormatStrFormatter('%.2e'))

    axes[1].set_title('Do nothing callbacks.')
    axes[1].legend(loc='best', frameon=True)
    axes[1].set_ylabel('Seconds')
    axes[1].get_yaxis().set_major_formatter(mtick.FormatStrFormatter('%.2e'))

    axes[2].set_title(
        'Do something callbacks. Pointer vs direct calls. Check for null ptr')
    axes[2].legend(loc='best', frameon=True)
    axes[2].set_ylabel('Seconds')
    axes[2].set_xlabel('Measurement')
    axes[2].get_yaxis().set_major_formatter(mtick.FormatStrFormatter('%.2e'))


analyze('x86_64', 'times.native.json')
analyze('Atmel328', 'times.arduino.json')
analyze('ARM Cortex-A8', 'times.arm.json')

plt.show()
