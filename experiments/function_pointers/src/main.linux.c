#include <soundvoid/basics.h>
#include <stdbool.h>

void function_pointers(bool arg, uint i, uint s, uint m);

int main(int argc, char** argv) {
  (void)argc;
  (void)argv;

  bool arg = false;
  if (argc > 1) {
    arg = true;
  }

  function_pointers(arg, 1e5, 1000, 100);

  return 0;
}
