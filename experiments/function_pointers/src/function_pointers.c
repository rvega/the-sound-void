#include "../../measure.h"
#include <math.h>
#include <soundvoid/print.h>
#include <stdbool.h>
#include <stdio.h>

typedef void cbk_type(uint a, uint b);

volatile uint tic = 0;
volatile uint toc = 0;

void do_tic(uint a, uint b) {
  tic = tic + a + b;
}

void do_nothing(uint a, uint b) {
  (void)a;
  (void)b;
  // nop
}

void function_pointers(bool arg, uint iters, uint samples, uint mments) {

  if (arg) {
    fprintf(stderr, "WHOA, STOP no args, please!\n");
  }

  uint i = 0;
  uint s = 0;
  uint m = 0;
  sv_print_char('[');

  // The arg parameter is here so the compiler doesn't optimize out the function
  // pointer calls. arg value cannot be inferred at compile time
  cbk_type* fcn_1 = null;

  /* The objective of this experiment is to compare the performance of having
   * callback functions implemented as normal function calls (this requires some
   * preprocessor tricks that are not common) vs callback functions implemented
   * as function pointers. Is any of the two really faster/slower?
   *
   * We'll look at these scenarios:
   *
   * 1. Simple comparison between fcn calls and pointers. */

  if (!arg) {
    fcn_1 = do_tic;
  }

  sv_measure_iterate_start("1.1 No fcn pointer", iters, i, samples, s, mments,
                           m);
  do_tic(m, m * 2);
  sv_measure_iterate_end(m);

  sv_print_char(',');

  sv_measure_iterate_start("1.2 With fcn pointer", iters, i, samples, s, mments,
                           m);
  fcn_1(m, m * 2);
  sv_measure_iterate_end(m);

  sv_print_char(',');

  /* 2. Optional Callbacks. When the callbacks are set to null, or we are
   * calling do_nothing() directly, is one of them faster than the other? The
   * optimizer will probably be able to remove code when calling do_nothing()
   * directly. */

  if (!arg) {
    fcn_1 = null;
  }
  sv_measure_iterate_start("2.1 fcn ptr to null", iters, i, samples, s, mments,
                           m);
  if (fcn_1) {
    fcn_1(m, m * 2);
  }
  sv_measure_iterate_end(m);

  sv_print_char(',');

  if (!arg) {
    fcn_1 = do_nothing;
  }
  sv_measure_iterate_start("2.2 fcn ptr to do_nothing", iters, i, samples, s,
                           mments, m);
  fcn_1(m, m * 2);
  sv_measure_iterate_end(m);

  sv_print_char(',');

  sv_measure_iterate_start("2.3 no fcn ptr do_nothing", iters, i, samples, s,
                           mments, m);
  do_nothing(m, m * 2);
  sv_measure_iterate_end(m);

  sv_print_char(',');

  /* 3. When the callbacks are set to a function that does actual work, or we
   * are calling do_tic() directly, is one of them faster than the
   * other? */
  if (!arg) {
    fcn_1 = do_tic;
  }
  sv_measure_iterate_start("3.1 fcn ptr to do_tic", iters, i, samples, s,
                           mments, m);
  if (fcn_1) {
    fcn_1(m, m * 2);
  }
  sv_measure_iterate_end(m);

  sv_print_char(',');

  sv_measure_iterate_start("3.2. do_tic directly", iters, i, samples, s, mments,
                           m);
  do_tic(m, m * 2);
  sv_measure_iterate_end(m);

  sv_print_str("\n\n]\n");
}

/* average_function *func; */
/* sv_measure_iterate_start("no_ptr_linear", iters, i, samples, s, mments, m);
 */
/* average_linear(table, increment * m); */
/* sv_measure_iterate_end(increment * m); */

/* sv_print_char(','); */

/* sv_measure_iterate_start("no_ptr_cubic", iters, i, samples, s, mments, m);
 */
/* average_cubic(table, increment * m); */
/* sv_measure_iterate_end(increment * m); */

/* sv_print_char(','); */

/* func = average_linear; */
/* sv_measure_iterate_start("ptr_linear", iters, i, samples, s, mments, m); */
/* func(table, increment * m); */
/* sv_measure_iterate_end(increment * m); */

/* sv_print_char(','); */

/* func = average_cubic; */
/* sv_measure_iterate_start("ptr_cubic", iters, i, samples, s, mments, m); */
/* func(table, increment * m); */
/* sv_measure_iterate_end(increment * m); */
/* float table[TABLE_SIZE + 2] = {0}; */
/* init_table(table); */
/* typedef float average_function(float*, float); */
/* void init_table(float *table) { */
/*   float pi = acos(-1); */
/*   float multiplier = 2.0 * pi / TABLE_SIZE; */
/*   for (uint i = 0; i < TABLE_SIZE; ++i) { */
/*     table[i] = sin(i * multiplier); */
/*   } */
/*   table[TABLE_SIZE] = table[0]; */
/*   table[TABLE_SIZE + 1] = table[1]; */
/* } */
/* float average_linear(float *table, float i) { */
/*   float index = i * TABLE_SIZE; */
/*   float fraction = index - (int)index; */
/*   float a = table[(int)index]; */
/*   float b = table[(int)index + 1]; */
/*   return (a + fraction * (b - a)); */
/* } */
/* float average_cubic(float *table, float i) { */
/*   float index = i * TABLE_SIZE; */
/*   float frac = index - (int)index; */
/*   float y0 = (int)index > 0 ? table[(int)index - 1] : table[TABLE_SIZE -
 * 1];
 */
/*   float y1 = table[(int)index]; */
/*   float y2 = table[(int)index + 1]; */
/*   float y3 = table[(int)index + 2]; */
/*   float tmp = y3 + 3.0 * y1; */
/*   float frac2 = pow(frac, 2); */
/*   float frac3 = pow(frac, 3); */
/*   return ((frac3 * (-y0 - 3.0 * y2 + tmp) / 6.0 + */
/*            frac2 * ((y0 + y2) / 2.0 - y1) + */
/*            frac * (y2 + (-2.0 * y0 - tmp) / 6.0) + y1)); */
/* } */
