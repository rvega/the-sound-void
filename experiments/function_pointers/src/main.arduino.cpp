#include <Arduino.h>
#include <soundvoid/basics.h>
#include <soundvoid/print.h>

extern "C" {
void function_pointers(bool args, uint i, uint s, uint m);
}

void setup() {
  sv_init();

  // this will always be false but the compiler has no way of knowing.
  bool args = Serial.available() == 1000;

  delay(1000);
  function_pointers(args, 1000, 100, 100);
}

void loop() {
}
