
build/fp_arm:     file format elf32-littlearm


Disassembly of section .init:

000103e4 <_init>:
   103e4:	e92d4008 	push	{r3, lr}
   103e8:	eb000034 	bl	104c0 <call_weak_fn>
   103ec:	e8bd8008 	pop	{r3, pc}

Disassembly of section .plt:

000103f0 <.plt>:
   103f0:	e52de004 	push	{lr}		; (str lr, [sp, #-4]!)
   103f4:	e59fe004 	ldr	lr, [pc, #4]	; 10400 <.plt+0x10>
   103f8:	e08fe00e 	add	lr, pc, lr
   103fc:	e5bef008 	ldr	pc, [lr, #8]!
   10400:	00010c00 	.word	0x00010c00

00010404 <printf@plt>:
   10404:	4778      	bx	pc
   10406:	e7fd      	b.n	10404 <printf@plt>
   10408:	e28fc600 	add	ip, pc, #0, 12
   1040c:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   10410:	e5bcfbfc 	ldr	pc, [ip, #3068]!	; 0xbfc

00010414 <clock_gettime@plt>:
   10414:	e28fc600 	add	ip, pc, #0, 12
   10418:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   1041c:	e5bcfbf4 	ldr	pc, [ip, #3060]!	; 0xbf4

00010420 <fwrite@plt>:
   10420:	e28fc600 	add	ip, pc, #0, 12
   10424:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   10428:	e5bcfbec 	ldr	pc, [ip, #3052]!	; 0xbec

0001042c <__libc_start_main@plt>:
   1042c:	e28fc600 	add	ip, pc, #0, 12
   10430:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   10434:	e5bcfbe4 	ldr	pc, [ip, #3044]!	; 0xbe4

00010438 <__gmon_start__@plt>:
   10438:	e28fc600 	add	ip, pc, #0, 12
   1043c:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   10440:	e5bcfbdc 	ldr	pc, [ip, #3036]!	; 0xbdc

00010444 <putc@plt>:
   10444:	4778      	bx	pc
   10446:	e7fd      	b.n	10444 <putc@plt>
   10448:	e28fc600 	add	ip, pc, #0, 12
   1044c:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   10450:	e5bcfbd0 	ldr	pc, [ip, #3024]!	; 0xbd0

00010454 <fputs@plt>:
   10454:	4778      	bx	pc
   10456:	e7fd      	b.n	10454 <fputs@plt>
   10458:	e28fc600 	add	ip, pc, #0, 12
   1045c:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   10460:	e5bcfbc4 	ldr	pc, [ip, #3012]!	; 0xbc4

00010464 <abort@plt>:
   10464:	e28fc600 	add	ip, pc, #0, 12
   10468:	e28cca10 	add	ip, ip, #16, 20	; 0x10000
   1046c:	e5bcfbbc 	ldr	pc, [ip, #3004]!	; 0xbbc

Disassembly of section .text:

00010470 <main>:
   10470:	2801      	cmp	r0, #1
   10472:	f44f 727a 	mov.w	r2, #1000	; 0x3e8
   10476:	b508      	push	{r3, lr}
   10478:	bfd4      	ite	le
   1047a:	2000      	movle	r0, #0
   1047c:	2001      	movgt	r0, #1
   1047e:	2364      	movs	r3, #100	; 0x64
   10480:	f248 61a0 	movw	r1, #34464	; 0x86a0
   10484:	f2c0 0101 	movt	r1, #1
   10488:	f000 f86e 	bl	10568 <function_pointers>
   1048c:	2000      	movs	r0, #0
   1048e:	bd08      	pop	{r3, pc}

00010490 <_start>:
   10490:	f04f 0b00 	mov.w	fp, #0
   10494:	f04f 0e00 	mov.w	lr, #0
   10498:	bc02      	pop	{r1}
   1049a:	466a      	mov	r2, sp
   1049c:	b404      	push	{r2}
   1049e:	b401      	push	{r0}
   104a0:	f8df c010 	ldr.w	ip, [pc, #16]	; 104b4 <_start+0x24>
   104a4:	f84d cd04 	str.w	ip, [sp, #-4]!
   104a8:	4803      	ldr	r0, [pc, #12]	; (104b8 <_start+0x28>)
   104aa:	4b04      	ldr	r3, [pc, #16]	; (104bc <_start+0x2c>)
   104ac:	f7ff efbe 	blx	1042c <__libc_start_main@plt>
   104b0:	f7ff efd8 	blx	10464 <abort@plt>
   104b4:	00010ce5 	.word	0x00010ce5
   104b8:	00010471 	.word	0x00010471
   104bc:	00010ca5 	.word	0x00010ca5

000104c0 <call_weak_fn>:
   104c0:	e59f3014 	ldr	r3, [pc, #20]	; 104dc <call_weak_fn+0x1c>
   104c4:	e59f2014 	ldr	r2, [pc, #20]	; 104e0 <call_weak_fn+0x20>
   104c8:	e08f3003 	add	r3, pc, r3
   104cc:	e7932002 	ldr	r2, [r3, r2]
   104d0:	e3520000 	cmp	r2, #0
   104d4:	012fff1e 	bxeq	lr
   104d8:	eaffffd6 	b	10438 <__gmon_start__@plt>
   104dc:	00010b30 	.word	0x00010b30
   104e0:	0000002c 	.word	0x0000002c

000104e4 <deregister_tm_clones>:
   104e4:	f241 0038 	movw	r0, #4152	; 0x1038
   104e8:	f2c0 0002 	movt	r0, #2
   104ec:	f241 0338 	movw	r3, #4152	; 0x1038
   104f0:	f2c0 0302 	movt	r3, #2
   104f4:	4283      	cmp	r3, r0
   104f6:	d005      	beq.n	10504 <deregister_tm_clones+0x20>
   104f8:	f240 0300 	movw	r3, #0
   104fc:	f2c0 0300 	movt	r3, #0
   10500:	b103      	cbz	r3, 10504 <deregister_tm_clones+0x20>
   10502:	4718      	bx	r3
   10504:	4770      	bx	lr
   10506:	bf00      	nop

00010508 <register_tm_clones>:
   10508:	f241 0038 	movw	r0, #4152	; 0x1038
   1050c:	f2c0 0002 	movt	r0, #2
   10510:	f241 0138 	movw	r1, #4152	; 0x1038
   10514:	f2c0 0102 	movt	r1, #2
   10518:	1a0b      	subs	r3, r1, r0
   1051a:	0fd9      	lsrs	r1, r3, #31
   1051c:	eb01 01a3 	add.w	r1, r1, r3, asr #2
   10520:	1049      	asrs	r1, r1, #1
   10522:	d005      	beq.n	10530 <register_tm_clones+0x28>
   10524:	f240 0300 	movw	r3, #0
   10528:	f2c0 0300 	movt	r3, #0
   1052c:	b103      	cbz	r3, 10530 <register_tm_clones+0x28>
   1052e:	4718      	bx	r3
   10530:	4770      	bx	lr
   10532:	bf00      	nop

00010534 <__do_global_dtors_aux>:
   10534:	b510      	push	{r4, lr}
   10536:	f241 0440 	movw	r4, #4160	; 0x1040
   1053a:	f2c0 0402 	movt	r4, #2
   1053e:	7823      	ldrb	r3, [r4, #0]
   10540:	b91b      	cbnz	r3, 1054a <__do_global_dtors_aux+0x16>
   10542:	f7ff ffcf 	bl	104e4 <deregister_tm_clones>
   10546:	2301      	movs	r3, #1
   10548:	7023      	strb	r3, [r4, #0]
   1054a:	bd10      	pop	{r4, pc}

0001054c <frame_dummy>:
   1054c:	e7dc      	b.n	10508 <register_tm_clones>
   1054e:	bf00      	nop

00010550 <do_tic>:
   10550:	f241 0244 	movw	r2, #4164	; 0x1044
   10554:	f2c0 0202 	movt	r2, #2
   10558:	6813      	ldr	r3, [r2, #0]
   1055a:	440b      	add	r3, r1
   1055c:	4403      	add	r3, r0
   1055e:	6013      	str	r3, [r2, #0]
   10560:	4770      	bx	lr
   10562:	bf00      	nop

00010564 <do_nothing>:
   10564:	4770      	bx	lr
   10566:	bf00      	nop

00010568 <function_pointers>:
   10568:	e92d 4ff0 	stmdb	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
   1056c:	b091      	sub	sp, #68	; 0x44
   1056e:	4690      	mov	r8, r2
   10570:	9103      	str	r1, [sp, #12]
   10572:	9304      	str	r3, [sp, #16]
   10574:	9005      	str	r0, [sp, #20]
   10576:	2800      	cmp	r0, #0
   10578:	f040 828f 	bne.w	10a9a <function_pointers+0x532>
   1057c:	205b      	movs	r0, #91	; 0x5b
   1057e:	f240 5451 	movw	r4, #1361	; 0x551
   10582:	f000 fb6b 	bl	10c5c <sv_print_char>
   10586:	f2c0 0401 	movt	r4, #1
   1058a:	9b04      	ldr	r3, [sp, #16]
   1058c:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   10590:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   10594:	a80b      	add	r0, sp, #44	; 0x2c
   10596:	9a03      	ldr	r2, [sp, #12]
   10598:	f640 5114 	movw	r1, #3348	; 0xd14
   1059c:	9300      	str	r3, [sp, #0]
   1059e:	f2c0 0101 	movt	r1, #1
   105a2:	4643      	mov	r3, r8
   105a4:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   105a8:	f10d 0b1c 	add.w	fp, sp, #28
   105ac:	f10d 0a24 	add.w	sl, sp, #36	; 0x24
   105b0:	edcd 0b07 	vstr	d16, [sp, #28]
   105b4:	2500      	movs	r5, #0
   105b6:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   105ba:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   105be:	f000 fa87 	bl	10ad0 <sv_measure_experiment_init>
   105c2:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   105c4:	b3b3      	cbz	r3, 10634 <function_pointers+0xcc>
   105c6:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   105c8:	006f      	lsls	r7, r5, #1
   105ca:	f04f 0900 	mov.w	r9, #0
   105ce:	b333      	cbz	r3, 1061e <function_pointers+0xb6>
   105d0:	4658      	mov	r0, fp
   105d2:	2600      	movs	r6, #0
   105d4:	f000 faf2 	bl	10bbc <sv_timespec_get>
   105d8:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   105da:	4628      	mov	r0, r5
   105dc:	4639      	mov	r1, r7
   105de:	b13b      	cbz	r3, 105f0 <function_pointers+0x88>
   105e0:	f7ff ffb6 	bl	10550 <do_tic>
   105e4:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   105e6:	3601      	adds	r6, #1
   105e8:	42b3      	cmp	r3, r6
   105ea:	d8f9      	bhi.n	105e0 <function_pointers+0x78>
   105ec:	4605      	mov	r5, r0
   105ee:	460f      	mov	r7, r1
   105f0:	4650      	mov	r0, sl
   105f2:	f109 0901 	add.w	r9, r9, #1
   105f6:	f000 fae1 	bl	10bbc <sv_timespec_get>
   105fa:	e89a 000c 	ldmia.w	sl, {r2, r3}
   105fe:	e89b 0003 	ldmia.w	fp, {r0, r1}
   10602:	f000 faf1 	bl	10be8 <sv_timespec_diff>
   10606:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   1060a:	a80b      	add	r0, sp, #44	; 0x2c
   1060c:	eef8 7a67 	vcvt.f32.u32	s15, s15
   10610:	ee80 0a27 	vdiv.f32	s0, s0, s15
   10614:	f000 fa7e 	bl	10b14 <sv_measure_add_sample>
   10618:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   1061a:	454b      	cmp	r3, r9
   1061c:	d8d8      	bhi.n	105d0 <function_pointers+0x68>
   1061e:	ee07 5a90 	vmov	s15, r5
   10622:	a80b      	add	r0, sp, #44	; 0x2c
   10624:	3501      	adds	r5, #1
   10626:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   1062a:	f000 fa8d 	bl	10b48 <sv_measure_add_measurement>
   1062e:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10630:	42ab      	cmp	r3, r5
   10632:	d8c8      	bhi.n	105c6 <function_pointers+0x5e>
   10634:	202c      	movs	r0, #44	; 0x2c
   10636:	f10d 0b1c 	add.w	fp, sp, #28
   1063a:	f000 fb0f 	bl	10c5c <sv_print_char>
   1063e:	9b04      	ldr	r3, [sp, #16]
   10640:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   10644:	a80b      	add	r0, sp, #44	; 0x2c
   10646:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   1064a:	9a03      	ldr	r2, [sp, #12]
   1064c:	9300      	str	r3, [sp, #0]
   1064e:	f640 5128 	movw	r1, #3368	; 0xd28
   10652:	4643      	mov	r3, r8
   10654:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   10658:	f2c0 0101 	movt	r1, #1
   1065c:	f10d 0a24 	add.w	sl, sp, #36	; 0x24
   10660:	edcd 0b07 	vstr	d16, [sp, #28]
   10664:	2500      	movs	r5, #0
   10666:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   1066a:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   1066e:	f000 fa2f 	bl	10ad0 <sv_measure_experiment_init>
   10672:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10674:	b39b      	cbz	r3, 106de <function_pointers+0x176>
   10676:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   10678:	006f      	lsls	r7, r5, #1
   1067a:	f04f 0900 	mov.w	r9, #0
   1067e:	b31b      	cbz	r3, 106c8 <function_pointers+0x160>
   10680:	4658      	mov	r0, fp
   10682:	2600      	movs	r6, #0
   10684:	f000 fa9a 	bl	10bbc <sv_timespec_get>
   10688:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   1068a:	b133      	cbz	r3, 1069a <function_pointers+0x132>
   1068c:	4639      	mov	r1, r7
   1068e:	4628      	mov	r0, r5
   10690:	47a0      	blx	r4
   10692:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   10694:	3601      	adds	r6, #1
   10696:	42b3      	cmp	r3, r6
   10698:	d8f8      	bhi.n	1068c <function_pointers+0x124>
   1069a:	4650      	mov	r0, sl
   1069c:	f109 0901 	add.w	r9, r9, #1
   106a0:	f000 fa8c 	bl	10bbc <sv_timespec_get>
   106a4:	e89a 000c 	ldmia.w	sl, {r2, r3}
   106a8:	e89b 0003 	ldmia.w	fp, {r0, r1}
   106ac:	f000 fa9c 	bl	10be8 <sv_timespec_diff>
   106b0:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   106b4:	a80b      	add	r0, sp, #44	; 0x2c
   106b6:	eef8 7a67 	vcvt.f32.u32	s15, s15
   106ba:	ee80 0a27 	vdiv.f32	s0, s0, s15
   106be:	f000 fa29 	bl	10b14 <sv_measure_add_sample>
   106c2:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   106c4:	454b      	cmp	r3, r9
   106c6:	d8db      	bhi.n	10680 <function_pointers+0x118>
   106c8:	ee07 5a90 	vmov	s15, r5
   106cc:	a80b      	add	r0, sp, #44	; 0x2c
   106ce:	3501      	adds	r5, #1
   106d0:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   106d4:	f000 fa38 	bl	10b48 <sv_measure_add_measurement>
   106d8:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   106da:	42ab      	cmp	r3, r5
   106dc:	d8cb      	bhi.n	10676 <function_pointers+0x10e>
   106de:	202c      	movs	r0, #44	; 0x2c
   106e0:	f000 fabc 	bl	10c5c <sv_print_char>
   106e4:	9b05      	ldr	r3, [sp, #20]
   106e6:	2b00      	cmp	r3, #0
   106e8:	f040 81b8 	bne.w	10a5c <function_pointers+0x4f4>
   106ec:	9b04      	ldr	r3, [sp, #16]
   106ee:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   106f2:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   106f6:	a80b      	add	r0, sp, #44	; 0x2c
   106f8:	9a03      	ldr	r2, [sp, #12]
   106fa:	f640 51b0 	movw	r1, #3504	; 0xdb0
   106fe:	9300      	str	r3, [sp, #0]
   10700:	f2c0 0101 	movt	r1, #1
   10704:	4643      	mov	r3, r8
   10706:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   1070a:	edcd 0b07 	vstr	d16, [sp, #28]
   1070e:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   10712:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   10716:	f000 f9db 	bl	10ad0 <sv_measure_experiment_init>
   1071a:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   1071c:	9c05      	ldr	r4, [sp, #20]
   1071e:	2b00      	cmp	r3, #0
   10720:	f000 81cd 	beq.w	10abe <function_pointers+0x556>
   10724:	f10d 0b1c 	add.w	fp, sp, #28
   10728:	f10d 0a24 	add.w	sl, sp, #36	; 0x24
   1072c:	2500      	movs	r5, #0
   1072e:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   10730:	006f      	lsls	r7, r5, #1
   10732:	f04f 0900 	mov.w	r9, #0
   10736:	b323      	cbz	r3, 10782 <function_pointers+0x21a>
   10738:	4658      	mov	r0, fp
   1073a:	f000 fa3f 	bl	10bbc <sv_timespec_get>
   1073e:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   10740:	b143      	cbz	r3, 10754 <function_pointers+0x1ec>
   10742:	b13c      	cbz	r4, 10754 <function_pointers+0x1ec>
   10744:	2600      	movs	r6, #0
   10746:	4639      	mov	r1, r7
   10748:	4628      	mov	r0, r5
   1074a:	47a0      	blx	r4
   1074c:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   1074e:	3601      	adds	r6, #1
   10750:	42b3      	cmp	r3, r6
   10752:	d8f8      	bhi.n	10746 <function_pointers+0x1de>
   10754:	4650      	mov	r0, sl
   10756:	f109 0901 	add.w	r9, r9, #1
   1075a:	f000 fa2f 	bl	10bbc <sv_timespec_get>
   1075e:	e89a 000c 	ldmia.w	sl, {r2, r3}
   10762:	e89b 0003 	ldmia.w	fp, {r0, r1}
   10766:	f000 fa3f 	bl	10be8 <sv_timespec_diff>
   1076a:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   1076e:	a80b      	add	r0, sp, #44	; 0x2c
   10770:	eef8 7a67 	vcvt.f32.u32	s15, s15
   10774:	ee80 0a27 	vdiv.f32	s0, s0, s15
   10778:	f000 f9cc 	bl	10b14 <sv_measure_add_sample>
   1077c:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   1077e:	454b      	cmp	r3, r9
   10780:	d8da      	bhi.n	10738 <function_pointers+0x1d0>
   10782:	ee07 5a90 	vmov	s15, r5
   10786:	a80b      	add	r0, sp, #44	; 0x2c
   10788:	3501      	adds	r5, #1
   1078a:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   1078e:	f000 f9db 	bl	10b48 <sv_measure_add_measurement>
   10792:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10794:	42ab      	cmp	r3, r5
   10796:	d8ca      	bhi.n	1072e <function_pointers+0x1c6>
   10798:	202c      	movs	r0, #44	; 0x2c
   1079a:	f000 fa5f 	bl	10c5c <sv_print_char>
   1079e:	9a05      	ldr	r2, [sp, #20]
   107a0:	f240 5365 	movw	r3, #1381	; 0x565
   107a4:	f2c0 0301 	movt	r3, #1
   107a8:	2a00      	cmp	r2, #0
   107aa:	bf08      	it	eq
   107ac:	461c      	moveq	r4, r3
   107ae:	9b04      	ldr	r3, [sp, #16]
   107b0:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   107b4:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   107b8:	a80b      	add	r0, sp, #44	; 0x2c
   107ba:	9a03      	ldr	r2, [sp, #12]
   107bc:	f640 5140 	movw	r1, #3392	; 0xd40
   107c0:	9300      	str	r3, [sp, #0]
   107c2:	f2c0 0101 	movt	r1, #1
   107c6:	4643      	mov	r3, r8
   107c8:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   107cc:	f10d 0b1c 	add.w	fp, sp, #28
   107d0:	f10d 0a24 	add.w	sl, sp, #36	; 0x24
   107d4:	edcd 0b07 	vstr	d16, [sp, #28]
   107d8:	2500      	movs	r5, #0
   107da:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   107de:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   107e2:	f000 f975 	bl	10ad0 <sv_measure_experiment_init>
   107e6:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   107e8:	b39b      	cbz	r3, 10852 <function_pointers+0x2ea>
   107ea:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   107ec:	006f      	lsls	r7, r5, #1
   107ee:	f04f 0900 	mov.w	r9, #0
   107f2:	b31b      	cbz	r3, 1083c <function_pointers+0x2d4>
   107f4:	4658      	mov	r0, fp
   107f6:	2600      	movs	r6, #0
   107f8:	f000 f9e0 	bl	10bbc <sv_timespec_get>
   107fc:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   107fe:	b133      	cbz	r3, 1080e <function_pointers+0x2a6>
   10800:	4639      	mov	r1, r7
   10802:	4628      	mov	r0, r5
   10804:	47a0      	blx	r4
   10806:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   10808:	3601      	adds	r6, #1
   1080a:	42b3      	cmp	r3, r6
   1080c:	d8f8      	bhi.n	10800 <function_pointers+0x298>
   1080e:	4650      	mov	r0, sl
   10810:	f109 0901 	add.w	r9, r9, #1
   10814:	f000 f9d2 	bl	10bbc <sv_timespec_get>
   10818:	e89a 000c 	ldmia.w	sl, {r2, r3}
   1081c:	e89b 0003 	ldmia.w	fp, {r0, r1}
   10820:	f000 f9e2 	bl	10be8 <sv_timespec_diff>
   10824:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   10828:	a80b      	add	r0, sp, #44	; 0x2c
   1082a:	eef8 7a67 	vcvt.f32.u32	s15, s15
   1082e:	ee80 0a27 	vdiv.f32	s0, s0, s15
   10832:	f000 f96f 	bl	10b14 <sv_measure_add_sample>
   10836:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   10838:	454b      	cmp	r3, r9
   1083a:	d8db      	bhi.n	107f4 <function_pointers+0x28c>
   1083c:	ee07 5a90 	vmov	s15, r5
   10840:	a80b      	add	r0, sp, #44	; 0x2c
   10842:	3501      	adds	r5, #1
   10844:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   10848:	f000 f97e 	bl	10b48 <sv_measure_add_measurement>
   1084c:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   1084e:	42ab      	cmp	r3, r5
   10850:	d8cb      	bhi.n	107ea <function_pointers+0x282>
   10852:	202c      	movs	r0, #44	; 0x2c
   10854:	f10d 091c 	add.w	r9, sp, #28
   10858:	f000 fa00 	bl	10c5c <sv_print_char>
   1085c:	9b04      	ldr	r3, [sp, #16]
   1085e:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   10862:	a80b      	add	r0, sp, #44	; 0x2c
   10864:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   10868:	9a03      	ldr	r2, [sp, #12]
   1086a:	9300      	str	r3, [sp, #0]
   1086c:	f640 515c 	movw	r1, #3420	; 0xd5c
   10870:	4643      	mov	r3, r8
   10872:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   10876:	f2c0 0101 	movt	r1, #1
   1087a:	ae09      	add	r6, sp, #36	; 0x24
   1087c:	edcd 0b07 	vstr	d16, [sp, #28]
   10880:	f04f 0a00 	mov.w	sl, #0
   10884:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   10888:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   1088c:	f000 f920 	bl	10ad0 <sv_measure_experiment_init>
   10890:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10892:	b33b      	cbz	r3, 108e4 <function_pointers+0x37c>
   10894:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   10896:	2500      	movs	r5, #0
   10898:	b1c3      	cbz	r3, 108cc <function_pointers+0x364>
   1089a:	4648      	mov	r0, r9
   1089c:	3501      	adds	r5, #1
   1089e:	f000 f98d 	bl	10bbc <sv_timespec_get>
   108a2:	4630      	mov	r0, r6
   108a4:	f000 f98a 	bl	10bbc <sv_timespec_get>
   108a8:	e896 000c 	ldmia.w	r6, {r2, r3}
   108ac:	e899 0003 	ldmia.w	r9, {r0, r1}
   108b0:	f000 f99a 	bl	10be8 <sv_timespec_diff>
   108b4:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   108b8:	a80b      	add	r0, sp, #44	; 0x2c
   108ba:	eef8 7a67 	vcvt.f32.u32	s15, s15
   108be:	ee80 0a27 	vdiv.f32	s0, s0, s15
   108c2:	f000 f927 	bl	10b14 <sv_measure_add_sample>
   108c6:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   108c8:	42ab      	cmp	r3, r5
   108ca:	d8e6      	bhi.n	1089a <function_pointers+0x332>
   108cc:	ee07 aa90 	vmov	s15, sl
   108d0:	a80b      	add	r0, sp, #44	; 0x2c
   108d2:	f10a 0a01 	add.w	sl, sl, #1
   108d6:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   108da:	f000 f935 	bl	10b48 <sv_measure_add_measurement>
   108de:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   108e0:	4553      	cmp	r3, sl
   108e2:	d8d7      	bhi.n	10894 <function_pointers+0x32c>
   108e4:	202c      	movs	r0, #44	; 0x2c
   108e6:	ad0b      	add	r5, sp, #44	; 0x2c
   108e8:	f000 f9b8 	bl	10c5c <sv_print_char>
   108ec:	9b04      	ldr	r3, [sp, #16]
   108ee:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   108f2:	f240 5051 	movw	r0, #1361	; 0x551
   108f6:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   108fa:	f2c0 0001 	movt	r0, #1
   108fe:	9300      	str	r3, [sp, #0]
   10900:	f640 5178 	movw	r1, #3448	; 0xd78
   10904:	f945 2a0f 	vst1.8	{d18-d19}, [r5]
   10908:	4643      	mov	r3, r8
   1090a:	9d05      	ldr	r5, [sp, #20]
   1090c:	f2c0 0101 	movt	r1, #1
   10910:	9a03      	ldr	r2, [sp, #12]
   10912:	f10d 0b1c 	add.w	fp, sp, #28
   10916:	2d00      	cmp	r5, #0
   10918:	bf08      	it	eq
   1091a:	4604      	moveq	r4, r0
   1091c:	a80b      	add	r0, sp, #44	; 0x2c
   1091e:	edcd 0b07 	vstr	d16, [sp, #28]
   10922:	f10d 0a24 	add.w	sl, sp, #36	; 0x24
   10926:	2500      	movs	r5, #0
   10928:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   1092c:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   10930:	f000 f8ce 	bl	10ad0 <sv_measure_experiment_init>
   10934:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10936:	b3a3      	cbz	r3, 109a2 <function_pointers+0x43a>
   10938:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   1093a:	006f      	lsls	r7, r5, #1
   1093c:	f04f 0900 	mov.w	r9, #0
   10940:	b323      	cbz	r3, 1098c <function_pointers+0x424>
   10942:	4658      	mov	r0, fp
   10944:	f000 f93a 	bl	10bbc <sv_timespec_get>
   10948:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   1094a:	b143      	cbz	r3, 1095e <function_pointers+0x3f6>
   1094c:	b13c      	cbz	r4, 1095e <function_pointers+0x3f6>
   1094e:	2600      	movs	r6, #0
   10950:	4639      	mov	r1, r7
   10952:	4628      	mov	r0, r5
   10954:	47a0      	blx	r4
   10956:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   10958:	3601      	adds	r6, #1
   1095a:	42b3      	cmp	r3, r6
   1095c:	d8f8      	bhi.n	10950 <function_pointers+0x3e8>
   1095e:	4650      	mov	r0, sl
   10960:	f109 0901 	add.w	r9, r9, #1
   10964:	f000 f92a 	bl	10bbc <sv_timespec_get>
   10968:	e89a 000c 	ldmia.w	sl, {r2, r3}
   1096c:	e89b 0003 	ldmia.w	fp, {r0, r1}
   10970:	f000 f93a 	bl	10be8 <sv_timespec_diff>
   10974:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   10978:	a80b      	add	r0, sp, #44	; 0x2c
   1097a:	eef8 7a67 	vcvt.f32.u32	s15, s15
   1097e:	ee80 0a27 	vdiv.f32	s0, s0, s15
   10982:	f000 f8c7 	bl	10b14 <sv_measure_add_sample>
   10986:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   10988:	454b      	cmp	r3, r9
   1098a:	d8da      	bhi.n	10942 <function_pointers+0x3da>
   1098c:	ee07 5a90 	vmov	s15, r5
   10990:	a80b      	add	r0, sp, #44	; 0x2c
   10992:	3501      	adds	r5, #1
   10994:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   10998:	f000 f8d6 	bl	10b48 <sv_measure_add_measurement>
   1099c:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   1099e:	42ab      	cmp	r3, r5
   109a0:	d8ca      	bhi.n	10938 <function_pointers+0x3d0>
   109a2:	202c      	movs	r0, #44	; 0x2c
   109a4:	f10d 091c 	add.w	r9, sp, #28
   109a8:	f000 f958 	bl	10c5c <sv_print_char>
   109ac:	9b04      	ldr	r3, [sp, #16]
   109ae:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   109b2:	a80b      	add	r0, sp, #44	; 0x2c
   109b4:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   109b8:	9a03      	ldr	r2, [sp, #12]
   109ba:	9300      	str	r3, [sp, #0]
   109bc:	f640 5190 	movw	r1, #3472	; 0xd90
   109c0:	4643      	mov	r3, r8
   109c2:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   109c6:	f2c0 0101 	movt	r1, #1
   109ca:	f10d 0824 	add.w	r8, sp, #36	; 0x24
   109ce:	edcd 0b07 	vstr	d16, [sp, #28]
   109d2:	2500      	movs	r5, #0
   109d4:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   109d8:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   109dc:	f000 f878 	bl	10ad0 <sv_measure_experiment_init>
   109e0:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   109e2:	b393      	cbz	r3, 10a4a <function_pointers+0x4e2>
   109e4:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   109e6:	006e      	lsls	r6, r5, #1
   109e8:	2700      	movs	r7, #0
   109ea:	b31b      	cbz	r3, 10a34 <function_pointers+0x4cc>
   109ec:	4648      	mov	r0, r9
   109ee:	2400      	movs	r4, #0
   109f0:	f000 f8e4 	bl	10bbc <sv_timespec_get>
   109f4:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   109f6:	b13b      	cbz	r3, 10a08 <function_pointers+0x4a0>
   109f8:	4631      	mov	r1, r6
   109fa:	4628      	mov	r0, r5
   109fc:	f7ff fda8 	bl	10550 <do_tic>
   10a00:	9b0b      	ldr	r3, [sp, #44]	; 0x2c
   10a02:	3401      	adds	r4, #1
   10a04:	42a3      	cmp	r3, r4
   10a06:	d8f7      	bhi.n	109f8 <function_pointers+0x490>
   10a08:	4640      	mov	r0, r8
   10a0a:	3701      	adds	r7, #1
   10a0c:	f000 f8d6 	bl	10bbc <sv_timespec_get>
   10a10:	e898 000c 	ldmia.w	r8, {r2, r3}
   10a14:	e899 0003 	ldmia.w	r9, {r0, r1}
   10a18:	f000 f8e6 	bl	10be8 <sv_timespec_diff>
   10a1c:	eddd 7a0b 	vldr	s15, [sp, #44]	; 0x2c
   10a20:	a80b      	add	r0, sp, #44	; 0x2c
   10a22:	eef8 7a67 	vcvt.f32.u32	s15, s15
   10a26:	ee80 0a27 	vdiv.f32	s0, s0, s15
   10a2a:	f000 f873 	bl	10b14 <sv_measure_add_sample>
   10a2e:	9b0e      	ldr	r3, [sp, #56]	; 0x38
   10a30:	42bb      	cmp	r3, r7
   10a32:	d8db      	bhi.n	109ec <function_pointers+0x484>
   10a34:	ee07 5a90 	vmov	s15, r5
   10a38:	a80b      	add	r0, sp, #44	; 0x2c
   10a3a:	3501      	adds	r5, #1
   10a3c:	eeb8 0a67 	vcvt.f32.u32	s0, s15
   10a40:	f000 f882 	bl	10b48 <sv_measure_add_measurement>
   10a44:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10a46:	42ab      	cmp	r3, r5
   10a48:	d8cc      	bhi.n	109e4 <function_pointers+0x47c>
   10a4a:	f640 50a8 	movw	r0, #3496	; 0xda8
   10a4e:	f2c0 0001 	movt	r0, #1
   10a52:	f000 f90b 	bl	10c6c <sv_print_str>
   10a56:	b011      	add	sp, #68	; 0x44
   10a58:	e8bd 8ff0 	ldmia.w	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, pc}
   10a5c:	9b04      	ldr	r3, [sp, #16]
   10a5e:	efc0 2050 	vmov.i32	q9, #0	; 0x00000000
   10a62:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   10a66:	a80b      	add	r0, sp, #44	; 0x2c
   10a68:	9a03      	ldr	r2, [sp, #12]
   10a6a:	f640 51b0 	movw	r1, #3504	; 0xdb0
   10a6e:	9300      	str	r3, [sp, #0]
   10a70:	f2c0 0101 	movt	r1, #1
   10a74:	4643      	mov	r3, r8
   10a76:	f940 2a0f 	vst1.8	{d18-d19}, [r0]
   10a7a:	edcd 0b07 	vstr	d16, [sp, #28]
   10a7e:	edcd 0b09 	vstr	d16, [sp, #36]	; 0x24
   10a82:	edcd 0b0e 	vstr	d16, [sp, #56]	; 0x38
   10a86:	f000 f823 	bl	10ad0 <sv_measure_experiment_init>
   10a8a:	9b0c      	ldr	r3, [sp, #48]	; 0x30
   10a8c:	2b00      	cmp	r3, #0
   10a8e:	f47f ae49 	bne.w	10724 <function_pointers+0x1bc>
   10a92:	202c      	movs	r0, #44	; 0x2c
   10a94:	f000 f8e2 	bl	10c5c <sv_print_char>
   10a98:	e689      	b.n	107ae <function_pointers+0x246>
   10a9a:	f241 0338 	movw	r3, #4152	; 0x1038
   10a9e:	f2c0 0302 	movt	r3, #2
   10aa2:	221c      	movs	r2, #28
   10aa4:	2101      	movs	r1, #1
   10aa6:	681b      	ldr	r3, [r3, #0]
   10aa8:	f640 40f4 	movw	r0, #3316	; 0xcf4
   10aac:	f2c0 0001 	movt	r0, #1
   10ab0:	2400      	movs	r4, #0
   10ab2:	f7ff ecb6 	blx	10420 <fwrite@plt>
   10ab6:	205b      	movs	r0, #91	; 0x5b
   10ab8:	f000 f8d0 	bl	10c5c <sv_print_char>
   10abc:	e565      	b.n	1058a <function_pointers+0x22>
   10abe:	202c      	movs	r0, #44	; 0x2c
   10ac0:	f240 5465 	movw	r4, #1381	; 0x565
   10ac4:	f000 f8ca 	bl	10c5c <sv_print_char>
   10ac8:	f2c0 0401 	movt	r4, #1
   10acc:	e66f      	b.n	107ae <function_pointers+0x246>
   10ace:	bf00      	nop

00010ad0 <sv_measure_experiment_init>:
   10ad0:	b570      	push	{r4, r5, r6, lr}
   10ad2:	4604      	mov	r4, r0
   10ad4:	9e04      	ldr	r6, [sp, #16]
   10ad6:	460d      	mov	r5, r1
   10ad8:	6002      	str	r2, [r0, #0]
   10ada:	60c3      	str	r3, [r0, #12]
   10adc:	2300      	movs	r3, #0
   10ade:	6046      	str	r6, [r0, #4]
   10ae0:	f640 50c4 	movw	r0, #3524	; 0xdc4
   10ae4:	60a3      	str	r3, [r4, #8]
   10ae6:	f2c0 0001 	movt	r0, #1
   10aea:	6123      	str	r3, [r4, #16]
   10aec:	f000 f8be 	bl	10c6c <sv_print_str>
   10af0:	4628      	mov	r0, r5
   10af2:	f000 f8bb 	bl	10c6c <sv_print_str>
   10af6:	f640 50d0 	movw	r0, #3536	; 0xdd0
   10afa:	f2c0 0001 	movt	r0, #1
   10afe:	f000 f8b5 	bl	10c6c <sv_print_str>
   10b02:	f640 50e0 	movw	r0, #3552	; 0xde0
   10b06:	e8bd 4070 	ldmia.w	sp!, {r4, r5, r6, lr}
   10b0a:	f2c0 0001 	movt	r0, #1
   10b0e:	f000 b8ad 	b.w	10c6c <sv_print_str>
   10b12:	bf00      	nop

00010b14 <sv_measure_add_sample>:
   10b14:	e9d0 3203 	ldrd	r3, r2, [r0, #12]
   10b18:	429a      	cmp	r2, r3
   10b1a:	d300      	bcc.n	10b1e <sv_measure_add_sample+0xa>
   10b1c:	4770      	bx	lr
   10b1e:	b510      	push	{r4, lr}
   10b20:	4604      	mov	r4, r0
   10b22:	200a      	movs	r0, #10
   10b24:	f000 f8b2 	bl	10c8c <sv_print_float>
   10b28:	e9d4 2303 	ldrd	r2, r3, [r4, #12]
   10b2c:	3301      	adds	r3, #1
   10b2e:	6123      	str	r3, [r4, #16]
   10b30:	4293      	cmp	r3, r2
   10b32:	d300      	bcc.n	10b36 <sv_measure_add_sample+0x22>
   10b34:	bd10      	pop	{r4, pc}
   10b36:	f640 50e8 	movw	r0, #3560	; 0xde8
   10b3a:	f2c0 0001 	movt	r0, #1
   10b3e:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
   10b42:	f000 b893 	b.w	10c6c <sv_print_str>
   10b46:	bf00      	nop

00010b48 <sv_measure_add_measurement>:
   10b48:	e9d0 2301 	ldrd	r2, r3, [r0, #4]
   10b4c:	4293      	cmp	r3, r2
   10b4e:	d300      	bcc.n	10b52 <sv_measure_add_measurement+0xa>
   10b50:	4770      	bx	lr
   10b52:	b510      	push	{r4, lr}
   10b54:	4604      	mov	r4, r0
   10b56:	ed2d 8b02 	vpush	{d8}
   10b5a:	3301      	adds	r3, #1
   10b5c:	2200      	movs	r2, #0
   10b5e:	6083      	str	r3, [r0, #8]
   10b60:	6122      	str	r2, [r4, #16]
   10b62:	f640 50ec 	movw	r0, #3564	; 0xdec
   10b66:	eeb0 8a40 	vmov.f32	s16, s0
   10b6a:	f2c0 0001 	movt	r0, #1
   10b6e:	f000 f87d 	bl	10c6c <sv_print_str>
   10b72:	200a      	movs	r0, #10
   10b74:	eeb0 0a48 	vmov.f32	s0, s16
   10b78:	f000 f888 	bl	10c8c <sv_print_float>
   10b7c:	f640 50f4 	movw	r0, #3572	; 0xdf4
   10b80:	f2c0 0001 	movt	r0, #1
   10b84:	f000 f872 	bl	10c6c <sv_print_str>
   10b88:	e9d4 3201 	ldrd	r3, r2, [r4, #4]
   10b8c:	429a      	cmp	r2, r3
   10b8e:	d209      	bcs.n	10ba4 <sv_measure_add_measurement+0x5c>
   10b90:	ecbd 8b02 	vpop	{d8}
   10b94:	f640 50f8 	movw	r0, #3576	; 0xdf8
   10b98:	f2c0 0001 	movt	r0, #1
   10b9c:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
   10ba0:	f000 b864 	b.w	10c6c <sv_print_str>
   10ba4:	ecbd 8b02 	vpop	{d8}
   10ba8:	f640 6004 	movw	r0, #3588	; 0xe04
   10bac:	f2c0 0001 	movt	r0, #1
   10bb0:	e8bd 4010 	ldmia.w	sp!, {r4, lr}
   10bb4:	f000 b85a 	b.w	10c6c <sv_print_str>

00010bb8 <sv_init>:
   10bb8:	2000      	movs	r0, #0
   10bba:	4770      	bx	lr

00010bbc <sv_timespec_get>:
   10bbc:	b510      	push	{r4, lr}
   10bbe:	efc0 0010 	vmov.i32	d16, #0	; 0x00000000
   10bc2:	b082      	sub	sp, #8
   10bc4:	4604      	mov	r4, r0
   10bc6:	2001      	movs	r0, #1
   10bc8:	4669      	mov	r1, sp
   10bca:	edcd 0b00 	vstr	d16, [sp]
   10bce:	f7ff ec22 	blx	10414 <clock_gettime@plt>
   10bd2:	3001      	adds	r0, #1
   10bd4:	bf08      	it	eq
   10bd6:	2001      	moveq	r0, #1
   10bd8:	d004      	beq.n	10be4 <sv_timespec_get+0x28>
   10bda:	eddd 0b00 	vldr	d16, [sp]
   10bde:	2000      	movs	r0, #0
   10be0:	f944 078f 	vst1.32	{d16}, [r4]
   10be4:	b002      	add	sp, #8
   10be6:	bd10      	pop	{r4, pc}

00010be8 <sv_timespec_diff>:
   10be8:	b410      	push	{r4}
   10bea:	b085      	sub	sp, #20
   10bec:	f10d 0c10 	add.w	ip, sp, #16
   10bf0:	466c      	mov	r4, sp
   10bf2:	e90c 0003 	stmdb	ip, {r0, r1}
   10bf6:	4610      	mov	r0, r2
   10bf8:	9903      	ldr	r1, [sp, #12]
   10bfa:	e884 000c 	stmia.w	r4, {r2, r3}
   10bfe:	4299      	cmp	r1, r3
   10c00:	9a02      	ldr	r2, [sp, #8]
   10c02:	d91c      	bls.n	10c3e <sv_timespec_diff+0x56>
   10c04:	1a5b      	subs	r3, r3, r1
   10c06:	eddf 0b12 	vldr	d16, [pc, #72]	; 10c50 <sv_timespec_diff+0x68>
   10c0a:	3801      	subs	r0, #1
   10c0c:	ee07 3a10 	vmov	s14, r3
   10c10:	1a83      	subs	r3, r0, r2
   10c12:	eeb8 7b47 	vcvt.f64.u32	d7, s14
   10c16:	ee00 3a10 	vmov	s0, r3
   10c1a:	ee37 7b20 	vadd.f64	d7, d7, d16
   10c1e:	eebc 7bc7 	vcvt.u32.f64	s14, d7
   10c22:	eddf 6a0d 	vldr	s13, [pc, #52]	; 10c58 <sv_timespec_diff+0x70>
   10c26:	eeb8 7a47 	vcvt.f32.u32	s14, s14
   10c2a:	eec7 7a26 	vdiv.f32	s15, s14, s13
   10c2e:	eeb8 0a40 	vcvt.f32.u32	s0, s0
   10c32:	ee37 0a80 	vadd.f32	s0, s15, s0
   10c36:	b005      	add	sp, #20
   10c38:	f85d 4b04 	ldr.w	r4, [sp], #4
   10c3c:	4770      	bx	lr
   10c3e:	1a82      	subs	r2, r0, r2
   10c40:	1a5b      	subs	r3, r3, r1
   10c42:	ee00 2a10 	vmov	s0, r2
   10c46:	ee07 3a10 	vmov	s14, r3
   10c4a:	e7ea      	b.n	10c22 <sv_timespec_diff+0x3a>
   10c4c:	f3af 8000 	nop.w
   10c50:	00000000 	.word	0x00000000
   10c54:	41cdcd65 	.word	0x41cdcd65
   10c58:	4e6e6b28 	.word	0x4e6e6b28

00010c5c <sv_print_char>:
   10c5c:	f241 033c 	movw	r3, #4156	; 0x103c
   10c60:	f2c0 0302 	movt	r3, #2
   10c64:	6819      	ldr	r1, [r3, #0]
   10c66:	f7ff bbed 	b.w	10444 <putc@plt>
   10c6a:	bf00      	nop

00010c6c <sv_print_str>:
   10c6c:	f241 033c 	movw	r3, #4156	; 0x103c
   10c70:	f2c0 0302 	movt	r3, #2
   10c74:	6819      	ldr	r1, [r3, #0]
   10c76:	f7ff bbed 	b.w	10454 <fputs@plt>
   10c7a:	bf00      	nop

00010c7c <sv_print_int>:
   10c7c:	4601      	mov	r1, r0
   10c7e:	f640 6008 	movw	r0, #3592	; 0xe08
   10c82:	f2c0 0001 	movt	r0, #1
   10c86:	f7ff bbbd 	b.w	10404 <printf@plt>
   10c8a:	bf00      	nop

00010c8c <sv_print_float>:
   10c8c:	eef7 0ac0 	vcvt.f64.f32	d16, s0
   10c90:	4601      	mov	r1, r0
   10c92:	f640 600c 	movw	r0, #3596	; 0xe0c
   10c96:	f2c0 0001 	movt	r0, #1
   10c9a:	ec53 2b30 	vmov	r2, r3, d16
   10c9e:	f7ff bbb1 	b.w	10404 <printf@plt>
   10ca2:	bf00      	nop

00010ca4 <__libc_csu_init>:
   10ca4:	e92d 43f8 	stmdb	sp!, {r3, r4, r5, r6, r7, r8, r9, lr}
   10ca8:	4607      	mov	r7, r0
   10caa:	4e0c      	ldr	r6, [pc, #48]	; (10cdc <__libc_csu_init+0x38>)
   10cac:	4688      	mov	r8, r1
   10cae:	4d0c      	ldr	r5, [pc, #48]	; (10ce0 <__libc_csu_init+0x3c>)
   10cb0:	4691      	mov	r9, r2
   10cb2:	447e      	add	r6, pc
   10cb4:	f7ff eb96 	blx	103e4 <_init>
   10cb8:	447d      	add	r5, pc
   10cba:	1b76      	subs	r6, r6, r5
   10cbc:	10b6      	asrs	r6, r6, #2
   10cbe:	d00a      	beq.n	10cd6 <__libc_csu_init+0x32>
   10cc0:	3d04      	subs	r5, #4
   10cc2:	2400      	movs	r4, #0
   10cc4:	f855 3f04 	ldr.w	r3, [r5, #4]!
   10cc8:	3401      	adds	r4, #1
   10cca:	464a      	mov	r2, r9
   10ccc:	4641      	mov	r1, r8
   10cce:	4638      	mov	r0, r7
   10cd0:	4798      	blx	r3
   10cd2:	42a6      	cmp	r6, r4
   10cd4:	d1f6      	bne.n	10cc4 <__libc_csu_init+0x20>
   10cd6:	e8bd 83f8 	ldmia.w	sp!, {r3, r4, r5, r6, r7, r8, r9, pc}
   10cda:	bf00      	nop
   10cdc:	00010256 	.word	0x00010256
   10ce0:	0001024c 	.word	0x0001024c

00010ce4 <__libc_csu_fini>:
   10ce4:	4770      	bx	lr
   10ce6:	bf00      	nop

Disassembly of section .fini:

00010ce8 <_fini>:
   10ce8:	e92d4008 	push	{r3, lr}
   10cec:	e8bd8008 	pop	{r3, pc}
