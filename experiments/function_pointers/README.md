# Implementing Callbacks. 

The objective of this experiment is to compare the performance of having callback functions implemented as normal function calls (this requires some preprocessor tricks that are not common) vs callback functions implemented as function pointers. Is any of the two really faster/slower?

Read function_pointers.c to see what we are actually measuring/comparing. 

To compile the experiment for the platform you are running (intel x86_64 linux in my case), for an ATMega328 microcontroller (Arduino Uno board), and for a Texas Instruments AM3358 ARM Cortex-A8 processor (Beaglebone board) use the scons build file. This will also generate readable assembly code for all platforms. Just run the scons command:

`scons`

Then, run the experiment and save the results in json format:

`scons run_native`
`scons run_arduino`
`scons run_arm`

Finally, analize the data with the py script:

`python analyze_times.py`

![1](1.png)
![2](2.png)
![3](3.png)

### Reading assembly code for Atmel328

Make sure the function calls are not being optimized away, you can use the sv_timespec_get calls as a guide and look for `call` and `icall` commands in there:

```asm
# 1.1 Call do_tic directly
     95e:	0e 94 38 02 	call	0x470	; 0x470 <sv_timespec_get>
     962:	89 81       	ldd	r24, Y+1	; 0x01
     964:	9a 81       	ldd	r25, Y+2	; 0x02
     966:	89 2b       	or	r24, r25
     968:	71 f0       	breq	.+28     	; 0x986 <__stack+0x87>
     96a:	e1 2c       	mov	r14, r1
     96c:	f1 2c       	mov	r15, r1
     96e:	b4 01       	movw	r22, r8
     970:	c5 01       	movw	r24, r10
     972:	0e 94 0a 01 	call	0x214	; 0x214 <do_tic>
     976:	3f ef       	ldi	r19, 0xFF	; 255
     978:	e3 1a       	sub	r14, r19
     97a:	f3 0a       	sbc	r15, r19
     97c:	89 81       	ldd	r24, Y+1	; 0x01
     97e:	9a 81       	ldd	r25, Y+2	; 0x02
     980:	e8 16       	cp	r14, r24
     982:	f9 06       	cpc	r15, r25
     984:	a0 f3       	brcs	.-24     	; 0x96e <__stack+0x6f>
     986:	ce 01       	movw	r24, r28
     988:	0b 96       	adiw	r24, 0x0b	; 11
     98a:	0e 94 38 02 	call	0x470	; 0x470 <sv_timespec_get>

# 1.2. Call do_tic through fcn_1 function pointer
     aa2:	0e 94 38 02 	call	0x470	; 0x470 <sv_timespec_get>
     aa6:	89 81       	ldd	r24, Y+1	; 0x01
     aa8:	9a 81       	ldd	r25, Y+2	; 0x02
     aaa:	89 2b       	or	r24, r25
     aac:	71 f0       	breq	.+28     	; 0xaca <__stack+0x1cb>
     aae:	e1 2c       	mov	r14, r1
     ab0:	f1 2c       	mov	r15, r1
     ab2:	b4 01       	movw	r22, r8
     ab4:	c5 01       	movw	r24, r10
     ab6:	f8 01       	movw	r30, r16
     ab8:	09 95       	icall
     aba:	ff ef       	ldi	r31, 0xFF	; 255
     abc:	ef 1a       	sub	r14, r31
     abe:	ff 0a       	sbc	r15, r31
     ac0:	89 81       	ldd	r24, Y+1	; 0x01
     ac2:	9a 81       	ldd	r25, Y+2	; 0x02
     ac4:	e8 16       	cp	r14, r24
     ac6:	f9 06       	cpc	r15, r25
     ac8:	a0 f3       	brcs	.-24     	; 0xab2 <__stack+0x1b3>
     aca:	ce 01       	movw	r24, r28
     acc:	0b 96       	adiw	r24, 0x0b	; 11
     ace:	0e 94 38 02 	call	0x470	; 0x470 <sv_timespec_get>
```

### For Intel x86-64

Similarly for Intel x86-64, look for `call`:

```asm
# 1.1 Call do_tic directly
    12ac:	e8 df 08 00 00       	call   1b90 <sv_timespec_get>
    12b1:	8b 7c 24 40          	mov    edi,DWORD PTR [rsp+0x40]
    12b5:	31 c0                	xor    eax,eax
    12b7:	85 ff                	test   edi,edi
    12b9:	74 18                	je     12d3 <function_pointers+0xe3>
    12bb:	0f 1f 44 00 00       	nop    DWORD PTR [rax+rax*1+0x0]
    12c0:	44 89 e6             	mov    esi,r12d
    12c3:	89 ef                	mov    edi,ebp
    12c5:	e8 f6 fe ff ff       	call   11c0 <do_tic>
    12ca:	83 c0 01             	add    eax,0x1
    12cd:	39 44 24 40          	cmp    DWORD PTR [rsp+0x40],eax
    12d1:	77 ed                	ja     12c0 <function_pointers+0xd0>
    12d3:	4c 89 f7             	mov    rdi,r14
    12d6:	41 83 c7 01          	add    r15d,0x1
    12da:	e8 b1 08 00 00       	call   1b90 <sv_timespec_get>
```

