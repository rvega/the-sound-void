****************
The Sound Void.
****************

The Sound Void is a library for writing musical applications for embedded devices. It was written in the C programming language, adhering to the C99 standard. 

For compilation instructions, API, and more detailed info, see the generated documentation (documentation/build/html).
