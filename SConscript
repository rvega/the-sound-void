import os

from CompilerFlags import *

#####################################################################
# Library compilation.

env = Environment(tools=['default'])

# Include directories (where to look for .h files) and compiler flags
env['CPPPATH'] = './include'.split()
env['CCFLAGS'] = sv_get_cflags(ARGUMENTS)
env['LINKFLAGS'] = sv_get_linkflags(ARGUMENTS)

# Source files
sources = 'midi_parser.c midi_encoder.c tuning.c poly.c'.split()
sources = ['src/' + i for i in sources]

# Build library for native linux
sources_linux = ['src/port.linux.c']
native = env.Clone()
native['CC'] = 'gcc'
library = native.Library('soundvoid', sources + sources_linux)

Default(library)

#####################################################################
# Tests. We use ceedling for building and running tests.

cflags_tests = """-DSV_MIDI_CBK_ALL -DSV_MIDI_CBK_TIMING_CLOCK -DSV_MIDI_CBK_CONTINUE
-DSV_MIDI_CBK_START -DSV_MIDI_CBK_STOP -DSV_MIDI_CBK_ACTIVE_SENSING
-DSV_MIDI_CBK_RESET -DSV_MIDI_CBK_NOTE_ON -DSV_MIDI_CBK_NOTE_OFF
-DSV_MIDI_CBK_PROGRAM_CHANGE -DSV_MIDI_CBK_POLIPHONIC_KEY_PRESSURE
-DSV_MIDI_CBK_CONTROL_CHANGE -DSV_MIDI_CBK_TUNE_REQUEST -DSV_MIDI_CBK_TIME_CODE
-DSV_MIDI_CBK_SONG_SELECT -DSV_MIDI_CBK_AFTERTOUCH -DSV_MIDI_CBK_PITCHBEND
-DSV_MIDI_CBK_SONG_POSITION -DSV_MIDI_CBK_CHANNEL_PRESSURE -DSV_MIDI_CBK_SYSEX
-DSV_MIDI_CBK_ALL_SOUND_OFF -DSV_MIDI_CBK_RESET_ALL_CONTROLLERS
-DSV_MIDI_CBK_LOCAL_CONTROL -DSV_MIDI_CBK_ALL_NOTES_OFF -DSV_MIDI_CBK_OMNI_OFF
-DSV_MIDI_CBK_OMNI_ON -DSV_MIDI_CBK_POLY_ON -DSV_MIDI_CBK_MONO_ON 
-DSV_POLY_CBK_NOTE_ON -DSV_POLY_CBK_NOTE_OFF""".split()

# Don't crash when compiler finds memory leaks, just report them
# https://stackoverflow.com/questions/22696071/
is_release = int(ARGUMENTS.get('release', 0))
if not is_release:
    os.environ['ASAN_OPTIONS'] = 'halt_on_error=0'

# Pass compiler flags to ceedling project. This will create project.yml file by
# replacing tokens like @C_COMPILER@ in the project.yml.in file.
replacements = {
    '@C_COMPILER@': native['CC'],
    '@C_PATH@': ' '.join(native['CPPPATH']),
    '@C_FLAGS@': ' '.join(native['CCFLAGS']),
    '@LINK_FLAGS@': ' '.join(native['LINKFLAGS']),
    '@C_FLAGS_TEST@': ' '.join(cflags_tests)
}
native.Substfile('#/tests/project.yml',
                 'tests/project.yml.in',
                 SUBST_DICT=replacements)

# Compile and run tests using ceedling
tests_command = native.Command(None, '#/tests/project.yml',
                               'cd tests && ./ceedling')
native.Clean(tests_command, '#/tests/build')
native.Alias('test', tests_command)
native.AlwaysBuild(tests_command)

#####################################################################
# Documentation

env.Command('documentation/build/html/index.html', 'documentation/Doxyfile',
            'cd documentation && make clean && doxygen && make html')
env.Alias('documentation', 'documentation/build/html/index.html')
env.AlwaysBuild('documentation')

# # vim: set ft=python :
