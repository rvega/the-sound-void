#include <alsa/asoundlib.h>
#include <alsa/seq_midi_event.h>

#include <soundvoid/basics.h>

///////////////////////////////////////////////////////////////////////
// VAINAS DE ALSA.
// Estas funciones son para pedirle al sistema operativo Linux los bytes de los
// mensajes MIDI que van llegando.

struct alsa_things {
  snd_seq_t* alsa_seq_handle;
  int in_port;
};
typedef struct alsa_things alsa_things;

// Inicializa el puerto MIDI de esta app. Retorna cero si todo OK.
void init_alsa(alsa_things* at) {
  snd_seq_t* seq_handle;
  int portid;

  if (snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_INPUT, 0) < 0) {
    fprintf(stderr, "Error opening ALSA sequencer.\n");
    exit(1);
  }
  snd_seq_set_client_name(seq_handle, "MIDI ALSA Example");
  if ((portid = snd_seq_create_simple_port(
           seq_handle, "MIDI ALSA Example",
           SND_SEQ_PORT_CAP_WRITE | SND_SEQ_PORT_CAP_SUBS_WRITE,
           SND_SEQ_PORT_TYPE_APPLICATION)) < 0) {
    fprintf(stderr, "Error creating MIDI port.\n");
    exit(1);
  }
  at->alsa_seq_handle = seq_handle;
  at->in_port = portid;
}

// Agarra los bytes MIDI que van llegando, los copia en buffer.
// y hace return de el numero de bytes que se copiaron.
uint read_alsa(alsa_things* at, uchar* buffer, uint buffer_size) {
  snd_seq_event_t* ev = null;

  // This function will block until a MIDI event arrives
  snd_seq_event_input(at->alsa_seq_handle, &ev);

  snd_midi_event_t* midi_ev;
  snd_midi_event_new(buffer_size, &midi_ev);
  uint n = snd_midi_event_decode(midi_ev, buffer, buffer_size, ev);
  return n;
}
