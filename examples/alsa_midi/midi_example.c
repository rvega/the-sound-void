#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "alsa_stuff.h" // Linux specific midi Input.
#include <soundvoid/midi.h>

///////////////////////////////////////////////////////////////////////
// CALLBACKS de nuestra libreria MIDI. Estas funciones son llamadas
// automaticamente por la libreria.

/* void midi_note_on_callback(uchar channel, uchar note, uchar velocity) { */
/*   printf("Llegó un NOTE ON con channel:%u, note:%u, velocity:%u \n", channel, */
/*          note, velocity); */
/* } */

/* void midi_note_off_callback(uchar channel, uchar note, uchar velocity) { */
/*   printf("Llegó un NOTE OFF con channel:%u, note:%u, velocity:%u \n", channel, */
/*          note, velocity); */
/* } */

/* void midi_program_change_callback(uchar channel, uchar program_number) { */
/*   printf("Llegó un PROGRAM CHANGE con channel:%u, program_number:%u \n", */
/*          channel, program_number); */
/* } */
/* void midi_poliphonic_key_pressure_callback(uchar channel, uchar note, */
/*                                            uchar velocity) { */
/*   printf("Llego un MIDI POLIPHONIC KEY PRESSURE con channel:%u, note:%u " */
/*          "velocity:%u \n", */
/*          channel, note, velocity); */
/* } */

/* void midi_control_change_callback(uchar channel, uchar note, uchar velocity) { */
/*   printf("Llego un MIDI CONTROL CHANGE con channel:%u, note:%u velocity:%u \n", */
/*          channel, note, velocity); */
/* } */

///////////////////////////////////////////////////////////////////////

int main() {
  alsa_things at = {0};
  init_alsa(&at);

  sv_midi_parser parser;
  sv_midi_parser_init(&parser);

  printf("starting loop\n");
  uchar buffer[3] = {0};
  while (true) {
    uint n = read_alsa(&at, buffer, 3);
    // En este punto, sabemos que tenemos que procesar los n bytes que se
    // encuentran almacenados en la variable buffer. Los procesamos con la
    // funcion de la librería
    sv_midi_parse(&parser, n, buffer);
  }

  return EXIT_SUCCESS;
}
