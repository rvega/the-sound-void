#include "unity.h"

#include <soundvoid/basics.h>
#include <soundvoid/midi_encoder.h>

uchar msg[5] = {0};
uint msg_size = 0;
sv_midi_encoder e = {0};

void setUp(void) {
  sv_midi_encoder_init(&e);
}

void test_running_status_mode(void) {
  /* When sending a bunch of the same messages, the status code should be
   * ommited and re-sent every 100th message.  */

  uchar channel = 1;
  uchar controller = 10;
  uchar value = 0;

  uint modulo = 100;
  for (uint i = 0; i < (modulo * 3) + 2; ++i) {
    value = i % 127;

    msg_size = sv_midi_enc_control_change(&e, channel, controller, value, msg);
    if (i % modulo == 0) {
      TEST_ASSERT_EQUAL(3, msg_size);
      TEST_ASSERT_EQUAL(0xB1, msg[0]);
      TEST_ASSERT_EQUAL(controller, msg[1]);
      TEST_ASSERT_EQUAL(value, msg[2]);
    }
    else {
      TEST_ASSERT_EQUAL(2, msg_size);
      TEST_ASSERT_EQUAL(controller, msg[0]);
      TEST_ASSERT_EQUAL(value, msg[1]);
    }
  }


  sv_midi_encoder_init(&e);
  for (uint i = 0; i < (modulo * 3) + 2; ++i) {
    value = i % 127;

    msg_size = sv_midi_enc_channel_aftertouch(&e, channel, value, msg);
    if (i % modulo == 0) {
      TEST_ASSERT_EQUAL(2, msg_size);
      TEST_ASSERT_EQUAL(0xD1, msg[0]);
      TEST_ASSERT_EQUAL(value, msg[1]);
    }
    else {
      TEST_ASSERT_EQUAL(1, msg_size);
      TEST_ASSERT_EQUAL(value, msg[0]);
    }
  }
}

void test_note_off(void) {
  uchar note = 65;
  uchar velocity = 100;
  uchar channel = 2;

  // note off messages with velocity != 0 send an actual note off message.
  // If velocity == 0, a note_on with velocity = 0 is sent.
  msg_size = sv_midi_enc_note_off(&e, channel, note, velocity, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0x82, msg[0]);
  TEST_ASSERT_EQUAL(note, msg[1]);
  TEST_ASSERT_EQUAL(velocity, msg[2]);

  msg_size = sv_midi_enc_note_off(&e, channel, note, 0, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0x92, msg[0]);
  TEST_ASSERT_EQUAL(note, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_note_on(void) {
  msg_size = sv_midi_enc_note_on(&e, 1, 65, 100, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0x91, msg[0]);
  TEST_ASSERT_EQUAL(65, msg[1]);
  TEST_ASSERT_EQUAL(100, msg[2]);
}

void test_poly_aftertouch(void) {
  msg_size = sv_midi_enc_poly_aftertouch(&e, 2, 66, 101, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xA2, msg[0]);
  TEST_ASSERT_EQUAL(66, msg[1]);
  TEST_ASSERT_EQUAL(101, msg[2]);
}

void test_control_change(void) {
  msg_size = sv_midi_enc_control_change(&e, 3, 67, 102, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xB3, msg[0]);
  TEST_ASSERT_EQUAL(67, msg[1]);
  TEST_ASSERT_EQUAL(102, msg[2]);
}

void test_control_change_precise(void) {
  msg_size = sv_midi_enc_control_change_precise(&e, 4, 3, 1024, msg);
  TEST_ASSERT_EQUAL(5, msg_size);
  TEST_ASSERT_EQUAL(0xB4, msg[0]);
  TEST_ASSERT_EQUAL(3, msg[1]);
  TEST_ASSERT_EQUAL(8, msg[2]);
  TEST_ASSERT_EQUAL(3 + 32, msg[3]);
  TEST_ASSERT_EQUAL(0, msg[4]);
}

void test_program_change(void) {
  msg_size = sv_midi_enc_program_change(&e, 5, 99, msg);
  TEST_ASSERT_EQUAL(2, msg_size);
  TEST_ASSERT_EQUAL(0xC5, msg[0]);
  TEST_ASSERT_EQUAL(99, msg[1]);
}

void test_channel_aftertouch(void) {
  msg_size = sv_midi_enc_channel_aftertouch(&e, 6, 20, msg);
  TEST_ASSERT_EQUAL(2, msg_size);
  TEST_ASSERT_EQUAL(0xD6, msg[0]);
  TEST_ASSERT_EQUAL(20, msg[1]);
}

void test_pitchbend(void) {
  msg_size = sv_midi_enc_pitchbend(&e, 7, 2000, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xE7, msg[0]);
  TEST_ASSERT_EQUAL(0x50, msg[1]);
  TEST_ASSERT_EQUAL(0x4F, msg[2]);

  msg_size = sv_midi_enc_pitchbend(&e, 7, -2000, msg);
  TEST_ASSERT_EQUAL(2, msg_size);
  TEST_ASSERT_EQUAL(0x30, msg[0]);
  TEST_ASSERT_EQUAL(0x30, msg[1]);
}

void test_all_sound_off(void) {
  msg_size = sv_midi_enc_all_sound_off(&e, 8, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xB8, msg[0]);
  TEST_ASSERT_EQUAL(120, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_reset_all_controllers(void) {
  msg_size = sv_midi_enc_reset_all_ctls(&e, 9, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xB9, msg[0]);
  TEST_ASSERT_EQUAL(121, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_local_control(void) {
  msg_size = sv_midi_enc_local_ctl(&e, 10, 127, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xBA, msg[0]);
  TEST_ASSERT_EQUAL(122, msg[1]);
  TEST_ASSERT_EQUAL(127, msg[2]);
}

void test_all_notes_off(void) {
  msg_size = sv_midi_enc_all_notes_off(&e, 11, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xBB, msg[0]);
  TEST_ASSERT_EQUAL(123, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_omni_off(void) {
  msg_size = sv_midi_enc_omni_off(&e, 12, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xBC, msg[0]);
  TEST_ASSERT_EQUAL(124, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_omni_on(void) {
  msg_size = sv_midi_enc_omni_on(&e, 13, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xBD, msg[0]);
  TEST_ASSERT_EQUAL(125, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_mono_on(void) {
  msg_size = sv_midi_enc_mono_on(&e, 14, 2, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xBE, msg[0]);
  TEST_ASSERT_EQUAL(126, msg[1]);
  TEST_ASSERT_EQUAL(2, msg[2]);
}

void test_poly_on(void) {
  msg_size = sv_midi_enc_poly_on(&e, 15, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xBF, msg[0]);
  TEST_ASSERT_EQUAL(127, msg[1]);
  TEST_ASSERT_EQUAL(0, msg[2]);
}

void test_start_sysex(void) {
  msg_size = sv_midi_enc_start_sysex(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xF0, msg[0]);
}

void test_time_code(void) {
  msg_size = sv_midi_enc_time_code(7, 15, msg);
  TEST_ASSERT_EQUAL(2, msg_size);
  TEST_ASSERT_EQUAL(0xF1, msg[0]);
  TEST_ASSERT_EQUAL(0x7F, msg[1]);
}

void test_song_position(void) {
  msg_size = sv_midi_enc_song_position(16256, msg);
  TEST_ASSERT_EQUAL(3, msg_size);
  TEST_ASSERT_EQUAL(0xF2, msg[0]);
  TEST_ASSERT_EQUAL(0x00, msg[1]);
  TEST_ASSERT_EQUAL(0x7F, msg[2]);
}

void test_song_select(void) {
  msg_size = sv_midi_enc_song_select(34, msg);
  TEST_ASSERT_EQUAL(2, msg_size);
  TEST_ASSERT_EQUAL(0xF3, msg[0]);
  TEST_ASSERT_EQUAL(34, msg[1]);
}

void test_tune_request(void) {
  msg_size = sv_midi_enc_tune_request(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xF6, msg[0]);
}

void test_end_sysex(void) {
  msg_size = sv_midi_enc_end_sysex(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xF7, msg[0]);
}

void test_timing_clock(void) {
  msg_size = sv_midi_enc_timing_clock(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xF8, msg[0]);
}

void test_start(void) {
  msg_size = sv_midi_enc_start(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xFA, msg[0]);
}

void test_continue(void) {
  msg_size = sv_midi_enc_continue(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xFB, msg[0]);
}

void test_stop(void) {
  msg_size = sv_midi_enc_stop(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xFC, msg[0]);
}

void test_active_sensing(void) {
  msg_size = sv_midi_enc_active_sensing(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xFE, msg[0]);
}

void test_reset(void) {
  msg_size = sv_midi_enc_reset(msg);
  TEST_ASSERT_EQUAL(1, msg_size);
  TEST_ASSERT_EQUAL(0xFF, msg[0]);
}
