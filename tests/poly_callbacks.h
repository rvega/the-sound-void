#include <soundvoid/basics.h>

void sv_poly_cbk_note_off(uint voice, uchar note, uchar velocity, void* user_data);
void sv_poly_cbk_note_on(uint voice, uchar note, uchar velocity, void* user_data);

