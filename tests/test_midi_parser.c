#include "mock_midi_callbacks.h"
#include "unity.h"

#include <soundvoid/midi_parser.h>

sv_midi_parser parser;

void setUp(void) {
  sv_midi_parser_init(&parser);
  // sv_midi_cbk_all_IgnoreAndReturn(true);
}

void test_cbk_all(void) {
  uchar incoming[] = {0x91, 0x40, 0xF8, 0x7F};
  uint n_incoming = 4;

  // When passing something to user_data, same pointer must be passed to
  // callbacks
  int user_data = 666;
  sv_midi_cbk_all_ExpectAndReturn(0xF8, 0x0, 0x0, &user_data, true);
  sv_midi_cbk_timing_clock_Expect(&user_data);
  sv_midi_cbk_all_ExpectAndReturn(0x91, 0x40, 0x7F, &user_data, true);
  sv_midi_cbk_note_on_Expect(1, 0x40, 0x7F, &user_data);
  sv_midi_parse(&parser, n_incoming, incoming, &user_data);

  /* // If cbk_all returns true, other callbacks should be triggered. */
  /* sv_midi_cbk_all_ExpectAndReturn(0xF8, null); */
  /* sv_midi_cbk_timing_clock_Expect(null); */
  /* sv_midi_cbk_all_ExpectAndReturn(0x91, 0x40, 0x7F, null, true); */
  /* sv_midi_cbk_note_on_Expect(1, 0x40, 0x7F, null); */
  /* sv_midi_parse(&parser, n_incoming, incoming, null); */

  /* // If cbk_all returns false, other callbacks should NOT be triggered. */
  /* sv_midi_cbk_all_ExpectAndReturn(0x91, 0x40, 0x7F, null, false); */
  /* sv_midi_parse(&parser, n_incoming, incoming, null); */
}

/* void test_filter(void) { */
/* } */

/*
void test_note_on(void) {
  // We are telling the test suite that the function sv_midi_cbk_note_on
  // should be called with the following parameter values:
  sv_midi_cbk_note_on_Expect(1, 0x40, 0x7F);
  uchar incoming[] = {0x91, 0x40, 0x7F};
  sv_midi_parse(&parser, 3, incoming);
}

void test_note_off(void) {
  sv_midi_cbk_note_off_Expect(1, 0x40, 0x7F);
  uchar incoming[] = {0x81, 0x40, 0x7F};
  sv_midi_parse(&parser, 3, incoming);

  sv_midi_cbk_note_off_Expect(1, 0x40, 0x00);
  uchar incoming2[] = {0x91, 0x40, 0x00};
  sv_midi_parse(&parser, 3, incoming2);
}

void test_program_change(void) {
  sv_midi_cbk_program_change_Expect(1, 0x21);
  uchar incoming[] = {0xC1, 0x21};
  sv_midi_parse(&parser, 2, incoming);
}

void test_fragmented_messages(void) {
  sv_midi_cbk_note_on_Expect(1, 0x40, 0x7F);
  sv_midi_cbk_note_off_Expect(1, 0x40, 0x00);

  uchar incoming1[] = {0x91};
  sv_midi_parse(&parser, 1, incoming1);

  uchar incoming2[] = {0x40, 0x7F};
  sv_midi_parse(&parser, 2, incoming2);

  uchar incoming3[] = {0x81, 0x40, 0x00};
  sv_midi_parse(&parser, 3, incoming3);
}

void test_real_time_midi_messages(void) {
  sv_midi_cbk_timing_clock_Expect();
  sv_midi_cbk_note_on_Expect(1, 0x40, 0x64);

  uchar incoming[] = {0x91, 0x40, 0xF8, 0x64};
  sv_midi_parse(&parser, 4, incoming);
}

void test_midi_running_status(void) {
  sv_midi_cbk_note_on_Expect(1, 0x40, 0x64);
  sv_midi_cbk_note_off_Expect(1, 0x40, 0x00);

  sv_midi_cbk_timing_clock_Expect();
  sv_midi_cbk_note_on_Expect(1, 0x41, 0x64);
  sv_midi_cbk_note_off_Expect(1, 0x41, 0x00);

  sv_midi_cbk_note_on_Expect(1, 0x42, 0x64);
  sv_midi_cbk_note_off_Expect(1, 0x42, 0x00);

  uchar incoming[] = {
      0x91, 0x40, 0x64, // Note on 40 64
      0x40, 0x00,       // Note off 40 0
      0x41, 0xF8, 0x64, // Note on 41 64 with timing clock in between
      0x41, 0x00,       //
      0x42, 0x64,       //
      0x42, 0x00};      //
  sv_midi_parse(&parser, sizeof(incoming), incoming);
}

void test_callback_all(void) {
  sv_midi_cbk_all_Expect(0x91, 0x40, 0x64);
  sv_midi_cbk_note_on_Expect(0x01, 0x40, 0x64);

  sv_midi_cbk_all_Expect(0x91, 0x40, 0x00);
  sv_midi_cbk_note_off_Expect(0x01, 0x40, 0x00);

  sv_midi_cbk_all_Expect(0xF8, 0x00, 0x00);
  sv_midi_cbk_timing_clock_Expect();
  sv_midi_cbk_all_Expect(0x91, 0x41, 0x64);
  sv_midi_cbk_note_on_Expect(0x01, 0x41, 0x64);

  sv_midi_cbk_all_Expect(0x91, 0x41, 0x00);
  sv_midi_cbk_note_off_Expect(0x01, 0x41, 0x00);

  sv_midi_cbk_all_Expect(0x91, 0x42, 0x64);
  sv_midi_cbk_note_on_Expect(0x01, 0x42, 0x64);

  sv_midi_cbk_all_Expect(0x91, 0x42, 0x00);
  sv_midi_cbk_note_off_Expect(0x01, 0x42, 0x00);

  uchar incoming[] = {
      0x91, 0x40, 0x64, // Note on 40 64
      0x40, 0x00,       // Note off 40 0
      0x41, 0xF8, 0x64, // Note on 41 64 with timing clock in between
      0x41, 0x00,       //
      0x42, 0x64,       //
      0x42, 0x00        //
  };
  sv_midi_parse(&parser, sizeof(incoming), incoming);
}

void test_omni_mode(void) {
  // If omni mode is false, only parse messages for the selected channel.
  // Messages that are not channel mode should not be affected.
  sv_midi_set_omni(&parser, false);
  sv_midi_set_channel(&parser, 3);

  // TODO!!!
  TEST_ASSERT_TRUE(false);
}

*/
