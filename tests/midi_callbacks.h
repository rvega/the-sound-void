#include <soundvoid/basics.h>
#include <stdbool.h>

bool sv_midi_cbk_all(uchar status, uchar data1, uchar data2, void *user_data);

void sv_midi_cbk_timing_clock(void* user_data);
void sv_midi_cbk_start(void* user_data);
void sv_midi_cbk_continue(void* user_data);
void sv_midi_cbk_stop(void* user_data);
void sv_midi_cbk_active_sensing(void* user_data);
void sv_midi_cbk_reset(void* user_data);

void sv_midi_cbk_note_on(uchar channel, uchar note, uchar velocity, void* user_data);
void sv_midi_cbk_note_off(uchar channel, uchar note, uchar velocity, void* user_data);
void sv_midi_cbk_program_change(uchar channel, uchar program_number, void* user_data);
void sv_midi_cbk_poliphonic_key_pressure(uchar channel, uchar note,
                                         uchar value, void* user_data);
void sv_midi_cbk_control_change(uchar channel, uchar note, uchar velocity, void* user_data);
void sv_midi_cbk_tune_request(void* user_data);
void sv_midi_cbk_time_code(uchar message_type, uchar value, void* user_data);
void sv_midi_cbk_song_select(uchar song, void* user_data);
void sv_midi_cbk_aftertouch(uchar channel, uchar note, uchar value, void* user_data);
void sv_midi_cbk_pitchbend(uchar channel, uint value, void* user_data);
void sv_midi_cbk_song_position(uchar value, void* user_data);
void sv_midi_cbk_channel_pressure(uchar channel, uchar value, void* user_data);

void sv_midi_cbk_sysex(uchar byte, void* user_data);

void sv_midi_cbk_reset_all_controllers(uchar channel, void* user_data);
void sv_midi_cbk_all_sound_off(uchar channel, void* user_data);
void sv_midi_cbk_local_control(uchar channel, bool local_control_on, void* user_data);
void sv_midi_cbk_all_notes_off(uchar channel, void* user_data);
void sv_midi_cbk_omni_on(uchar channel, void* user_data);
void sv_midi_cbk_omni_off(uchar channel, void* user_data);
void sv_midi_cbk_mono_on(uchar channel, void* user_data);
void sv_midi_cbk_poly_on(uchar channel, void* user_data);
