#include "mock_poly_callbacks.h"
#include "unity.h"

#include <stdbool.h>

#include <soundvoid/basics.h>
#include <soundvoid/poly.h>

void setUp(void) {
}

void test_poly_monophonic(void) {
  int data = 99;
  sv_poly p;
  sv_poly_init(&p);
  sv_poly_set_num_voices(&p, 1);
  sv_poly_set_user_data(&p, &data);

  // simple note on and off
  sv_poly_cbk_note_on_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 60, 20, &data);
  sv_poly_note_on(&p, 60, 100);
  sv_poly_note_off(&p, 60, 20);
  
  // hold one note and play with second note 2 times and then release first one
  sv_poly_cbk_note_on_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 60, 100, &data);
  sv_poly_note_on(&p, 60, 100);
  sv_poly_note_on(&p, 62, 100);
  sv_poly_note_off(&p, 62, 100);
  sv_poly_note_on(&p, 62, 100);
  sv_poly_note_off(&p, 62, 100);
  sv_poly_note_off(&p, 60, 100);
  
  // hold several and release them 
  sv_poly_cbk_note_on_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 64, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 64, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 66, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 66, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 64, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 64, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 62, 100, &data);
  sv_poly_cbk_note_on_Expect(0, 60, 100, &data);
  sv_poly_cbk_note_off_Expect(0, 60, 100, &data);
  sv_poly_note_on(&p, 60, 100);
  sv_poly_note_on(&p, 62, 100);
  sv_poly_note_on(&p, 64, 100);
  sv_poly_note_on(&p, 66, 100);
  sv_poly_note_off(&p, 66, 100);
  sv_poly_note_off(&p, 64, 100);
  sv_poly_note_off(&p, 62, 100);
  sv_poly_note_off(&p, 60, 100);
}
