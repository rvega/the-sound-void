#include <assert.h>
#include <stdbool.h>

#include "soundvoid/midi.h"
#include "soundvoid/midi_parser.h"

///////////////////////////////////////////////////////////////////////////////
// NOTES / TODOS:
//
// * Correct errors in documentation.
// * Test omni mode.
// * Test callback all.
// * Test any status byte except real time messages should end sysex.
// * Clear running status byte when any system common message arrives.
// * Running status should only be set when incoming message is actually for this channel.
// * if omni is on, ignore reset_all_controllers
//
// * Implement mono/poly mode.
// * CC controllers from 0 to 31 receive an optional LSB with controllers 32
//   to 63.
// * Implement Registered Parameters, see Continuous Control table/spec

///////////////////////////////////////////////////////////////////////////////
// "Private" functions
//

static bool sv_midi_status_equals(uchar status_byte, uchar message_type) {
  return ((status_byte & 0xF0) == message_type);
}

static bool sv_midi_is_status_byte(uchar byte) {
  return ((byte & 0x80) == 0x80);
}

static bool sv_midi_get_channel(uchar byte) {
  return (0x0F & byte);
}

static bool sv_midi_match_channel(bool omni, uchar current_chan, uchar incoming_chan) {
  if (omni || current_chan == incoming_chan) {
    return true;
  }
  else {
    return false;
  }
}

static bool sv_midi_handle_rt_message(uchar byte, void* user_data) {
  if (byte == SV_MIDI_TIMING_CLOCK) {
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_timing_clock(user_data);
    }
    return true;
  }
  if (byte == SV_MIDI_START) {
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_start(user_data);
    }
    return true;
  }
  if (byte == SV_MIDI_CONTINUE) {
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_continue(user_data);
    }
    return true;
  }
  if (byte == SV_MIDI_STOP) {
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_stop(user_data);
    }
    return true;
  }
  if (byte == SV_MIDI_ACTIVE_SENSING) {
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_active_sensing(user_data);
    }
    return true;
  }
  if (byte == SV_MIDI_STATUS_RESET) {
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_reset(user_data);
    }
    return true;
  }
  if (byte == SV_MIDI_TUNE_REQUEST) {
    // tune request is not really a real time message but it's the only message
    // appart from real time and sysex messages that is 1 byte long. Putting
    // it here simplifies the logic of the parse function and does no harm
    // (interleaving tune requests with multi byte messages won't happen).
    if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
      sv_midi_cbk_tune_request(user_data);
    }
    return true;
  }

  return false;
}

static uchar sv_midi_get_num_data_bytes(uchar status) {
  if (sv_midi_status_equals(status, SV_MIDI_NOTE_OFF) || sv_midi_status_equals(status, SV_MIDI_NOTE_ON) ||
      sv_midi_status_equals(status, SV_MIDI_AFTERTOUCH) || sv_midi_status_equals(status, SV_MIDI_CONTROL_CHANGE) ||
      sv_midi_status_equals(status, SV_MIDI_PITCH_BEND) || //
      status == SV_MIDI_SONG_POSITION) {
    return 2;
  }
  else if (sv_midi_status_equals(status, SV_MIDI_PROGRAM_CHANGE) ||
           sv_midi_status_equals(status, SV_MIDI_CHANNEL_PRESSURE) || //
           status == SV_MIDI_TIME_CODE || status == SV_MIDI_SONG_SELECT) {
    return 1;
  }

  return 0;
}

static void sv_midi_handle_2_byte_message(const sv_midi_parser* x, uchar byte, void* user_data) {
  uchar status = x->previous_bytes[0];

  if (!sv_midi_cbk_all(status, byte, 0, user_data)) {
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_PROGRAM_CHANGE)) {
    uchar channel = sv_midi_get_channel(status);
    uchar patch = byte;

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    sv_midi_cbk_program_change(channel, patch, user_data);
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_CHANNEL_PRESSURE)) {
    uchar channel = sv_midi_get_channel(status);
    uchar value = byte;

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    sv_midi_cbk_channel_pressure(channel, value, user_data);
    return;
  }

  if (status == SV_MIDI_TIME_CODE) {
    uchar message_type = (byte & 0x70) >> 4;
    uchar values = byte & 0x0F;
    sv_midi_cbk_time_code(message_type, values, user_data);
    return;
  }

  if (status == SV_MIDI_SONG_SELECT) {
    uchar song = byte;
    sv_midi_cbk_song_select(song, user_data);
    return;
  }

  assert(false && "Unknown 2 byte midi message?");
}

static void sv_midi_handle_3_byte_message(sv_midi_parser* x, uchar byte, void* user_data) {
  uchar status = x->previous_bytes[0];

  // TODO: look at this carefully when using LSB and MSB
  if (!sv_midi_cbk_all(status, x->previous_bytes[1], byte, user_data)) {
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_NOTE_OFF)) {
    uchar channel = sv_midi_get_channel(status);
    uchar note = x->previous_bytes[1];
    uchar velocity = byte;

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    sv_midi_cbk_note_off(channel, note, velocity, user_data);
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_NOTE_ON)) {
    uchar channel = sv_midi_get_channel(status);
    uchar note = x->previous_bytes[1];
    uchar velocity = byte;

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    if (velocity == 0) {
      sv_midi_cbk_note_off(channel, note, velocity, user_data);
    }
    else {
      sv_midi_cbk_note_on(channel, note, velocity, user_data);
    }
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_AFTERTOUCH)) {
    uchar channel = sv_midi_get_channel(status);
    uchar note = x->previous_bytes[1];
    uchar value = byte;

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    sv_midi_cbk_aftertouch(channel, note, value, user_data);
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_CONTROL_CHANGE)) {
    uchar channel = sv_midi_get_channel(status);
    uchar controller = x->previous_bytes[1];
    uchar value = byte;

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    switch (controller) {
      case SV_MIDI_ALL_SOUND_OFF:
        sv_midi_cbk_all_sound_off(channel, user_data);
        break;
      case SV_MIDI_RESET_ALL_CONTROLLERS:
        sv_midi_cbk_reset_all_controllers(channel, user_data);
        break;
      case SV_MIDI_LOCAL_CONTROL:
        if (value == 0) {
          sv_midi_cbk_local_control(channel, false, user_data);
        }
        else if (value == 127) {
          sv_midi_cbk_local_control(channel, true, user_data);
        }
        break;
      case SV_MIDI_ALL_NOTES_OFF:
        sv_midi_cbk_all_notes_off(channel, user_data);
        break;
      case SV_MIDI_OMNI_OFF:
        x->omni = false;
        sv_midi_cbk_all_notes_off(channel, user_data);
        sv_midi_cbk_omni_off(channel, user_data);
        break;
      case SV_MIDI_OMNI_ON:
        x->omni = true;
        sv_midi_cbk_all_notes_off(channel, user_data);
        sv_midi_cbk_omni_on(channel, user_data);
        break;
      case SV_MIDI_MONO_ON:
        sv_midi_cbk_all_notes_off(channel, user_data);
        sv_midi_cbk_mono_on(channel, user_data);
        break;
      case SV_MIDI_POLY_ON:
        sv_midi_cbk_all_notes_off(channel, user_data);
        sv_midi_cbk_poly_on(channel, user_data);
        break;
      default:
        sv_midi_cbk_control_change(channel, controller, value, user_data);
        break;
    }
    return;
  }

  if (sv_midi_status_equals(status, SV_MIDI_PITCH_BEND)) {
    uchar channel = sv_midi_get_channel(status);
    uchar lsb = x->previous_bytes[1];
    uchar msb = byte;
    int value = ((msb << 7) + lsb) - 0x2000; // convert to 14 bit signed integer

    if (!sv_midi_match_channel(x->omni, x->channel, channel)) {
      return;
    }

    sv_midi_cbk_pitchbend(channel, value, user_data);
    return;
  }

  if (status == SV_MIDI_SONG_POSITION) {
    uchar lsb = x->previous_bytes[1];
    uchar msb = byte;
    uint value = ((msb << 7) + lsb); // convert to 14 bit unsigned integer
    sv_midi_cbk_song_position(value, user_data);
    return;
  }

  assert(false && "Unknown 3 byte midi message?");
}

///////////////////////////////////////////////////////////////////////////////
// "Public" functions
//

void sv_midi_parse(sv_midi_parser* x, uint n, const uchar* bytes, void* user_data) {
  for (uint i = 0; i < n; ++i) {
    uchar byte = bytes[i];

    ////////////////////////////
    // 0. Check if byte is a system real time message. These messages are 1 byte
    // long and can be interleaved with bytes from other messages.
    // Just dispatch it.
    if (sv_midi_handle_rt_message(byte, user_data)) {
      continue;
    }

    ////////////////////////////
    // 1. If we're expecting a status byte but we get a data byte instead, do
    // the midi running status thing.
    if (x->state == SV_MIDI_STATE_STATUS && !sv_midi_is_status_byte(byte)) {
      x->previous_bytes[0] = x->running_status;
      if (x->expected_data_bytes == 2) {
        x->state = SV_MIDI_STATE_DATA_1_OF_2;
      }
      else if (x->expected_data_bytes == 1) {
        x->state = SV_MIDI_STATE_DATA_1_OF_1;
      }
    }

    ////////////////////////////
    // 2. If the first bit of the byte is 1, this is a status byte,
    // it's the start of a new message.
    if (sv_midi_is_status_byte(byte)) {
      x->previous_bytes[0] = byte;
      x->running_status = byte;
      x->expected_data_bytes = sv_midi_get_num_data_bytes(x->previous_bytes[0]);
      if (x->expected_data_bytes == 2) {
        x->state = SV_MIDI_STATE_DATA_1_OF_2;
        continue;
      }
      else if (x->expected_data_bytes == 1) {
        x->state = SV_MIDI_STATE_DATA_1_OF_1;
        continue;
      }
      else { // x->expected_data_bytes == 0
        if (byte == SV_MIDI_START_SYSEX) {
          x->state = SV_MIDI_STATE_SYSEX;
          if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
            sv_midi_cbk_sysex(byte, user_data);
          }
          continue;
        }
        if (byte == SV_MIDI_END_SYSEX) {
          x->state = SV_MIDI_STATE_STATUS;
          if (sv_midi_cbk_all(byte, 0, 0, user_data)) {
            sv_midi_cbk_sysex(byte, user_data);
          }
          continue;
        }
      }
    }

    ////////////////////////////
    // 3. First byte of data, just store it.
    if (x->state == SV_MIDI_STATE_DATA_1_OF_2) {
      x->previous_bytes[1] = byte;
      x->state = SV_MIDI_STATE_DATA_2_OF_2;
      continue;
    }

    ////////////////////////////
    // 4. Last byte of data, dispatch message
    if (x->state == SV_MIDI_STATE_DATA_2_OF_2) {
      x->state = SV_MIDI_STATE_STATUS; // Next state is wait for status byte
      sv_midi_handle_3_byte_message(x, byte, user_data);
      continue;
    }
    if (x->state == SV_MIDI_STATE_DATA_1_OF_1) {
      x->state = SV_MIDI_STATE_STATUS; // Next state is wait for status byte
      sv_midi_handle_2_byte_message(x, byte, user_data);
      continue;
    }

    ////////////////////////////
    // 5. If we're receiving sysex butes, dispatch them one by one.
    if (x->state == SV_MIDI_STATE_SYSEX) {
      sv_midi_cbk_sysex(byte, user_data);
      sv_midi_cbk_all(byte, 0, 0, user_data);
      continue;
    }
  }
}

void sv_midi_set_omni(sv_midi_parser* x, bool omni_mode) {
  x->omni = omni_mode;
}

void sv_midi_set_channel(sv_midi_parser* x, uint channel) {
  x->channel = channel;
}

void sv_midi_parser_init(sv_midi_parser* x) {
  for (uint i = 0; i < 2; ++i) {
    x->previous_bytes[i] = 0;
  }
  x->running_status = 0;
  x->omni = true;
  x->channel = 1;
  x->state = SV_MIDI_STATE_RESET;
}
