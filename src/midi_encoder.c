#include <assert.h>
#include <stdbool.h>

#include "soundvoid/midi_encoder.h"

// The idea is to re-send the status
// code every few seconds, so let's say
// user is sending sixteenth notes at
// 120bpm, that's 64 messages per
// second. Let's re-send the status code
// every 100 messages. This is all
// arbitrary.
static bool sv_midi_enc_should_send_status(sv_midi_encoder* x, uchar status_byte) {
  uint count_thresshold = 100;

  if (x->previous_status_byte != status_byte || x->running_status_counter >= count_thresshold - 1) {
    x->running_status_counter = 0;
    x->previous_status_byte = status_byte;
    return true;
  }
  else {
    x->running_status_counter++;
    return false;
  }
}

static uint sv_midi_enc_2bytes_chan(sv_midi_encoder* x, uchar channel, uchar status, uchar b1,
                                    uchar* bytes) {
  assert(channel < 16 && "Channel must be less than 16");
  assert(b1 <= 127 && "Byte 1 must be less than 127");
  assert(bytes && "Bytes can't be null");
  assert(x && "X can't be null");
  assert(status && "Status can't be zero");

  uchar status_byte = status | channel;
  if (sv_midi_enc_should_send_status(x, status_byte)) {
    bytes[0] = status_byte;
    bytes[1] = b1;
    return 2;
  }
  else {
    bytes[0] = b1;
    return 1;
  }
}

static uint sv_midi_enc_3bytes_chan(sv_midi_encoder* x, uchar channel, uchar status, uchar b1,
                                    uchar b2, uchar* bytes) {
  assert(channel < 16 && "Channel must be less than 16");
  assert(b1 <= 127 && "Byte 1 must be less than 127");
  assert(b2 <= 127 && "Byte 2 must be less than 127");
  assert(bytes && "Bytes can't be null");
  assert(x && "X can't be null");
  assert(status && "Status can't be zero");

  uchar status_byte = status | channel;
  if (sv_midi_enc_should_send_status(x, status_byte)) {
    bytes[0] = status_byte;
    bytes[1] = b1;
    bytes[2] = b2;
    return 3;
  }
  else {
    bytes[0] = b1;
    bytes[1] = b2;
    return 2;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Public API:
//

void sv_midi_encoder_init(sv_midi_encoder* x) {
  x->previous_status_byte = 0;
  x->running_status_counter = 0;
}

uint sv_midi_enc_note_off(sv_midi_encoder* x, uchar channel, uchar note, uchar velocity,
                          uchar* bytes) {
  // if velocity == 0, send note-on
  // status. This will save data with
  // running status mode.
  if (velocity == 0) {
    return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_NOTE_ON, note, velocity, bytes);
  }
  // Else, send note off status. User
  // probably needs the velocity.
  else {
    return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_NOTE_OFF, note, velocity, bytes);
  }
}

uint sv_midi_enc_note_on(sv_midi_encoder* x, uchar channel, uchar note, uchar velocity,
                         uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_NOTE_ON, note, velocity, bytes);
}

uint sv_midi_enc_poly_aftertouch(sv_midi_encoder* x, uchar channel, uchar note, uchar value,
                                 uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_POLY_AFTERTOUCH, note, value, bytes);
}

uint sv_midi_enc_control_change(sv_midi_encoder* x, uchar channel, uchar controller, uint value,
                                uchar* bytes) {
  assert(controller < 120 && "Controller number must be less than 120");
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, controller, value, bytes);
}

uint sv_midi_enc_control_change_precise(sv_midi_encoder* x, uchar channel, uchar controller,
                                        uint value, uchar* bytes) {
  assert(controller < 32 && "Controller number must be less than 32 for precise CCs");
  uchar lsb = value & 0x3FF;
  uchar msb = value >> 7;
  uint n1 = sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, controller, msb, bytes);
  uint n2 =
      sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, controller + 32, lsb, &bytes[n1]);
  return n1 + n2;
}

uint sv_midi_enc_program_change(sv_midi_encoder* x, uchar channel, uchar program, uchar* bytes) {
  return sv_midi_enc_2bytes_chan(x, channel, SV_MIDI_PROGRAM_CHANGE, program, bytes);
}

uint sv_midi_enc_channel_aftertouch(sv_midi_encoder* x, uchar channel, uchar value, uchar* bytes) {
  return sv_midi_enc_2bytes_chan(x, channel, SV_MIDI_CHANNEL_AFTERTOUCH, value, bytes);
}

uint sv_midi_enc_pitchbend(sv_midi_encoder* x, uchar channel, int value, uchar* bytes) {
  assert((value >= -8192 || value <= 8191) && "Pitchbend between -8192 and 8191");

  // Center value is 0, min value is
  // -8192, max value is 8191. 0 is sent
  // as 0x2000, max is sent as 0x3FFF,
  // min is sent as 0x0000.
  int val = value + 8192;
  uchar lsb = val & 0x7F;
  uchar msb = val >> 7;
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_PITCH_BEND, lsb, msb, bytes);
}

uint sv_midi_enc_all_sound_off(sv_midi_encoder* x, uchar channel, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_ALL_SOUND_OFF, 0,
                                 bytes);
}

uint sv_midi_enc_reset_all_ctls(sv_midi_encoder* x, uchar channel, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_RESET_ALL_CONTROLLERS,
                                 0, bytes);
}

uint sv_midi_enc_local_ctl(sv_midi_encoder* x, uchar channel, bool local_control_on, uchar* bytes) {
  uchar val = 0;
  if (local_control_on) {
    val = 127;
  }
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_LOCAL_CONTROL, val,
                                 bytes);
}

uint sv_midi_enc_all_notes_off(sv_midi_encoder* x, uchar channel, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_ALL_NOTES_OFF, 0,
                                 bytes);
}

uint sv_midi_enc_omni_off(sv_midi_encoder* x, uchar channel, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_OMNI_OFF, 0, bytes);
}

uint sv_midi_enc_omni_on(sv_midi_encoder* x, uchar channel, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_OMNI_ON, 0, bytes);
}

uint sv_midi_enc_mono_on(sv_midi_encoder* x, uchar channel, uchar num_channels, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_MONO_ON, num_channels,
                                 bytes);
}

uint sv_midi_enc_poly_on(sv_midi_encoder* x, uchar channel, uchar* bytes) {
  return sv_midi_enc_3bytes_chan(x, channel, SV_MIDI_CONTROL_CHANGE, SV_MIDI_POLY_ON, 0, bytes);
}

uint sv_midi_enc_start_sysex(uchar* bytes) {
  bytes[0] = SV_MIDI_START_SYSEX;
  return 1;
}

uint sv_midi_enc_time_code(uchar message_type, uchar value, uchar* bytes) {
  assert(message_type < 8 && "Time code message type less "
                             "than 8");
  assert(value < 16 && "Time code values less than 16");

  bytes[0] = SV_MIDI_TIME_CODE;
  bytes[1] = ((message_type & 0x7) << 4) | (value & 0xF);

  return 2;
}

uint sv_midi_enc_song_position(uint value, uchar* bytes) {
  assert(value < 0x3FFF && "song position value less "
                           "than 0x3FFF (16383)");
  uchar lsb = value & 0x7F;
  uchar msb = value >> 7;
  bytes[0] = SV_MIDI_SONG_POSITION;
  bytes[1] = lsb;
  bytes[2] = msb;
  return 3;
}

uint sv_midi_enc_song_select(uchar song, uchar* bytes) {
  assert(song <= 127 && "song position value less "
                        "than 127");
  bytes[0] = SV_MIDI_SONG_SELECT;
  bytes[1] = song;
  return 2;
}

uint sv_midi_enc_tune_request(uchar* bytes) {
  bytes[0] = SV_MIDI_TUNE_REQUEST;
  return 1;
}

uint sv_midi_enc_end_sysex(uchar* bytes) {
  bytes[0] = SV_MIDI_END_SYSEX;
  return 1;
}

uint sv_midi_enc_timing_clock(uchar* bytes) {
  bytes[0] = SV_MIDI_TIMING_CLOCK;
  return 1;
}

uint sv_midi_enc_continue(uchar* bytes) {
  bytes[0] = SV_MIDI_CONTINUE;
  return 1;
}

uint sv_midi_enc_start(uchar* bytes) {
  bytes[0] = SV_MIDI_START;
  return 1;
}

uint sv_midi_enc_stop(uchar* bytes) {
  bytes[0] = SV_MIDI_STOP;
  return 1;
}

uint sv_midi_enc_active_sensing(uchar* bytes) {
  bytes[0] = SV_MIDI_ACTIVE_SENSING;
  return 1;
}

uint sv_midi_enc_reset(uchar* bytes) {
  bytes[0] = SV_MIDI_STATUS_RESET;
  return 1;
}
