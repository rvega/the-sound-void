#ifdef __sv_arduino__

#include <Arduino.h>
#include <soundvoid/time.h>
#include <soundvoid/print.h>
#include <soundvoid/basics.h>

/////////////////////////////////////////////////////////////////////
// Implementation of basics.h

int sv_init(void) {
  Serial.begin(115200);
  return 0;
}

/////////////////////////////////////////////////////////////////////
// Implementation of time.h

int sv_timespec_get(sv_timespec* ts) {
  ts->tv_sec = 0;
  ts->tv_nsec = micros();
  return 0;
}

float sv_timespec_diff(sv_timespec t1, sv_timespec t2) {
  sv_timespec diff = {0};
  diff.tv_sec = t2.tv_sec - t1.tv_sec;
  diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
  return (diff.tv_sec + diff.tv_nsec / 1e6f);
}

/////////////////////////////////////////////////////////////////////
// Implementation of print.h

void sv_print_char(char c) {
  Serial.write(c);
}

void sv_print_str(char* str) {
  Serial.write(str);
}

void sv_print_int(int i) {
  Serial.print(i);
}

void sv_print_float(float f, int digits) {
  if (digits > 24) {
    digits = 24;
  }
  char str[32] = {0};
  dtostre(f, str, digits, 0);
  Serial.write(str);
}

// Print message  and halt program execution when assert checks fail.
void __assert(const char* __func, const char* __file, int __lineno,
              const char* __sexp) {
  Serial.println(__func);
  Serial.println(__file);
  Serial.println(__lineno, DEC);
  Serial.println(__sexp);
  Serial.flush();
  abort();
}

#endif // __arduino__
