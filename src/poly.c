#include <assert.h>
#include <limits.h>

#include "soundvoid/poly.h"

// TODO: Eventually, the voices and notes arrays could be implemented as hashmaps, I have a pretty
// fast hashmap implementation in C that is small enough but it uses malloc. I'll eventually get rid
// of those dynamic allocations and bring it here.

static void sv_poly_zero(sv_poly* x) {
  x->counter_notes = 0;
  x->counter_voices = 0;

  x->active_voices = 0;
  for (uint i = 0; i < SV_POLY_MAX_VOICES; ++i) {
    x->voices[i].note = 0;
    x->voices[i].order = UINT_MAX;
    x->voices[i].active = false;
  }

  x->active_notes = 0;
  for (uint i = 0; i < SV_POLY_MAX_NOTES; ++i) {
    x->notes[i].note = 0;
    x->notes[i].order = UINT_MAX;
    x->notes[i].active = false;
  }
}

void sv_poly_init(sv_poly* x) {
  x->num_voices = 1;
  sv_poly_zero(x);
}

void sv_poly_reset(sv_poly* x) {
  sv_poly_zero(x);
}

void sv_poly_set_num_voices(sv_poly* x, uint n) {
  assert(n < SV_POLY_MAX_VOICES);
  sv_poly_reset(x);
  x->num_voices = n;
}

void sv_poly_set_user_data(sv_poly* restrict x, void* restrict user_data) {
  x->user_data = user_data;
}

static inline void sv_poly_find_oldest(sv_poly_node const* restrict array, uint array_size,
                                       int* restrict index) {
  *index = -1;
  uint min_order = UINT_MAX;
  for (uint i = 0; i < array_size; ++i) {
    uint order = array[i].order;
    if (order <= min_order) {
      min_order = order;
      *index = i;
    }
  }
}

static inline void sv_poly_find_oldest_inactive(sv_poly_node const* restrict array, uint array_size,
                                                int* restrict index) {
  *index = -1;
  uint min_order = UINT_MAX;
  for (uint i = 0; i < array_size; ++i) {
    uint order = array[i].order;
    bool active = array[i].active;
    if (order <= min_order && !active) {
      min_order = order;
      *index = i;
    }
  }
}

static inline void sv_poly_find_newest_active(sv_poly_node const* restrict array, uint array_size,
                                       int* restrict index) {
  *index = -1;
  uint max_order = 0;
  for (uint i = 0; i < array_size; ++i) {
    uint order = array[i].order;
    bool active = array[i].active;
    if (order >= max_order && active) {
      max_order = order;
      *index = i;
    }
  }
}

static inline void sv_poly_find_inactive(sv_poly_node const* restrict array, uint array_size,
                                         int* restrict index) {
  *index = -1;
  for (uint i = 0; i < array_size; ++i) {
    if (!array[i].active) {
      *index = i;
      break;
    }
  }
}

static inline void sv_poly_find_note(sv_poly_node const* restrict array, uint array_size,
                                     uchar note, int* restrict index) {
  *index = -1;
  for (uint i = 0; i < array_size; ++i) {
    if (array[i].note == note) {
      *index = i;
      break;
    }
  }
}

void sv_poly_note_on(sv_poly* x, uchar note, uchar velocity) {
  // 1. Store the new note.
  // if we dont have enough space for new note, use the slot of the oldest note.
  int i = -1;
  if (x->active_notes >= SV_POLY_MAX_NOTES) {
    sv_poly_find_oldest(x->notes, SV_POLY_MAX_NOTES, &i);
  }
  // Else, we have space, find any inactive slot.
  else {
    sv_poly_find_inactive(x->notes, SV_POLY_MAX_NOTES, &i);
  }
  assert(i != -1);
  sv_poly_node* new_note = &x->notes[i];
  new_note->note = note;
  new_note->order = x->counter_notes;
  new_note->active = true;
  x->active_notes++;
  x->counter_notes++;

  // 2. Turn on a voice for the new note.
  // Not enough voices, turn off oldest
  if (x->active_voices >= x->num_voices) {
    sv_poly_find_oldest(x->voices, x->num_voices, &i);
    sv_poly_node* old_voice = &x->voices[i];
    old_voice->active = false;
    x->active_voices--;
    uchar old_note = old_voice->note;
    sv_poly_cbk_note_off(i, old_note, velocity, x->user_data);
  }
  // There are enough voices, turn one on.
  else {
    sv_poly_find_oldest_inactive(x->voices, x->num_voices, &i);
  }
  assert(i != -1);
  sv_poly_node* voice = &x->voices[i];
  voice->note = note;
  voice->order = x->counter_voices;
  voice->active = true;
  x->active_voices++;
  x->counter_voices++;
  sv_poly_cbk_note_on(i, note, velocity, x->user_data);
}

void sv_poly_note_off(sv_poly* x, uchar note, uchar velocity) {
  // 1. Deactivate voice
  int i = -1;
  sv_poly_find_note(x->voices, x->num_voices, note, &i);
  if (i == -1) {
    return;
  }
  sv_poly_cbk_note_off(i, note, velocity, x->user_data);
  sv_poly_node* voice = &x->voices[i];
  voice->active = false;
  x->active_voices--;

  // 2. Deactivate note
  sv_poly_find_note(x->notes, SV_POLY_MAX_NOTES, note, &i);
  if (i == -1) {
    return;
  }
  sv_poly_node* n = &x->notes[i];
  n->active = false;
  x->active_notes--;

  // 3. Monosynth thingy
  if (x->num_voices == 1 && x->active_notes > 0) {
    // find the note we should play
    sv_poly_find_newest_active(x->notes, SV_POLY_MAX_NOTES, &i);
    if (i == -1) {
      return;
    }
    uchar nn = x->notes[i].note;

    // play it
    sv_poly_find_oldest_inactive(x->voices, x->num_voices, &i);
    assert(i != -1);
    sv_poly_node* voice = &x->voices[i];
    voice->note = nn;
    voice->order = x->counter_voices;
    voice->active = true;
    x->active_voices++;
    x->counter_voices++;
    sv_poly_cbk_note_on(i, nn, velocity, x->user_data);
  }
}
