#if __linux__

#define _POSIX_C_SOURCE 199309L // Needed for clock_gettime()
#include <soundvoid/time.h>
#include <stdio.h>
#include <time.h>

/////////////////////////////////////////////////////////////////////

int sv_init(void) {
  // nop
  return 0;
}

/////////////////////////////////////////////////////////////////////
// Implementation of time.h

int sv_timespec_get(sv_timespec* ts) {
  struct timespec now = {0};
  if (clock_gettime(CLOCK_MONOTONIC, &now) == -1) {
    return 1;
  }
  ts->tv_sec = now.tv_sec;
  ts->tv_nsec = now.tv_nsec;

  return 0;
}

float sv_timespec_diff(sv_timespec t1, sv_timespec t2) {
  sv_timespec diff = {0};
  if (t2.tv_nsec < t1.tv_nsec) {
    diff.tv_sec = t2.tv_sec - t1.tv_sec - 1;
    diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1e9;
  }
  else {
    diff.tv_sec = t2.tv_sec - t1.tv_sec;
    diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
  }
  return (diff.tv_sec + diff.tv_nsec / 1e9f);
}

/////////////////////////////////////////////////////////////////////
// Implementation of print.h

void sv_print_char(char c) {
  putchar(c);
}

void sv_print_str(char* str) {
  fputs(str, stdout);
}

void sv_print_int(int i) {
  printf("%i", i);
}

void sv_print_float(float f, int digits) {
  printf("%.*e", digits, f);
}

#endif // __linux__
