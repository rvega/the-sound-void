#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "soundvoid/basics.h"
#include "soundvoid/tuning.h"

#define SV_TUNING_MAX_LINE_LEN 256
#define SV_TUNING_NOTE_RESET 1e10

// Modifies a string, removes whitespace.
static void sv_tuning_trim(char** p) {
  char* str = *p;
  uint len = strlen(str);
  uint start = 0;
  uint end = len;

  for (uint i = 0; i < len; ++i) {
    if (!isspace(str[i])) {
      break;
    }
    start++;
  }

  for (uint i = len - 1; i > start; --i) {
    if (!isspace(str[i])) {
      break;
    }
    end--;
  }

  str[end] = '\0';
  *p = &(str[start]);
}

static int sv_tuning_parse_line(char* line, uint line_len, uint* restrict note,
                                float* restrict value) {
  // Look for the '=' character.
  uint i = 0;
  while (i < line_len) {
    char c = line[i];
    if (c == '=') {
      break;
    }
    else if (c == '\0') {
      // Not found!
      return -1;
    }
    i++;
  }

  line[i] = '\0';

  *note = atoi(&line[4]);
  *value = atof(&line[i + 1]);
  return 0;
}

static void sv_tuning_reset_table(float* table, uint n) {
  for (uint i = 0; i < n; ++i) {
    table[i] = SV_TUNING_NOTE_RESET;
  }
}

static float sv_tuning_parse_base_freq(char* line, uint line_len) {
  // Look for the '=' character.
  uint i = 0;
  while (i < line_len) {
    char c = line[i];
    if (c == '=') {
      break;
    }
    else if (c == '\0') {
      // Not found!
      return -1.0;
    }
    i++;
  }

  float value = atof(&line[i + 1]);
  return value;
}

static int sv_tuning_read_file(FILE* f, bool* restrict is_exact,
                               float* restrict notes, uint notes_length,
                               float* restrict base_freq) {
  assert(notes_length == 128);
  assert(notes);
  assert(f);

  // Reset notes
  *base_freq = -1.0;
  sv_tuning_reset_table(notes, 128);

  // A buffer to hold each line of the file.
  char buf[SV_TUNING_MAX_LINE_LEN] = {0};
  char* line = &buf[0];

  // Parser State variable.
  // 0 Means we're not in a not-interesting section.
  // 1 Means we're in [Tuning] section.
  // 2 Means we're in [Exact Tuning] section.
  uint state = 0;
  uint i = 0;
  while (fgets(line, SV_TUNING_MAX_LINE_LEN, f)) {
    sv_tuning_trim(&line);

    // Empty string?
    // Comment?
    if (line[0] == '\0' || line[0] == ';') {
      i++;
      continue;
    }

    // Start of section?
    else if (line[0] == '[') {
      if (strncmp(line, "[Tuning]", 8) == 0) {
        sv_tuning_reset_table(notes, 128);
        state = 1;
      }
      else if (strncmp(line, "[Exact Tuning]", 14) == 0) {
        sv_tuning_reset_table(notes, 128);
        *is_exact = true;
        state = 2;
      }
      else {
        state = 0;
      }
    }

    // If we're in either [Tuning] or [Exact Tuning], put the individual note
    // values in the array. [Exact Tuning] has more priority.
    else if ((state == 2 || (state == 1 && !*is_exact)) &&
             (strncmp("note", line, 4) == 0)) {
      uint note = 0;
      float value = 0.0;
      if(sv_tuning_parse_line(line, SV_TUNING_MAX_LINE_LEN, &note, &value)) {
        return -1;
      }
      notes[note] = value;
    }

    // If we're in [Exact Tuning], get BaseFreq value.
    else if (state == 2 && (strncmp(line, "BaseFreq", 8) == 0)) {
      *base_freq = sv_tuning_parse_base_freq(line, SV_TUNING_MAX_LINE_LEN);
    }

    i++;
  }

  return 0;
}

static float sv_tuning_cents_to_hz(float cents, float base_freq) {
  if (base_freq == -1.0) {
    // base_freq is the frequency for midi note 0 in a standard A4=440Hz tuning.
    // const float base_freq = 440.0 * pow(2.0, -69.0/12.0);
    base_freq = 8.1757989156437073336;
  }
  float hz = base_freq * pow(2.0, cents / 1200.0);
  return hz;
}

static int sv_tuning_calculate_freqs(float* restrict table, uint table_size,
                                     float* restrict note_definitions,
                                     uint note_definitions_size, bool is_exact,
                                     float base_freq) {
  assert(table_size == 128);
  assert(note_definitions_size == 128);
  assert(table);
  assert(note_definitions);

  // The note definitions come from a '[Tuning]' section. All 128 notes must be
  // present.
  if (!is_exact) {
    for (uint i = 0; i < note_definitions_size; ++i) {
      float cents = note_definitions[i];

      // There no valid value for the note. Use the default
      if (cents == SV_TUNING_NOTE_RESET) {
        cents = 100.0 * i;
      }

      float hz = sv_tuning_cents_to_hz(cents, base_freq);
      table[i] = hz;
    }
  }

  // The note definitions come from a '[Exact Tuning]' section. There is
  // "autocompletion", see specification doc.
  else {
    uint highest_defined_h = 0;
    float highest_defined_p = 0.0;
    for (uint i = 0; i < note_definitions_size; ++i) {
      float cents = note_definitions[i];

      // In this case, each note is represented as an offset in cents from an
      // A4=440Hz standard tuning. base_freq is the frequency for midi note 0 in
      // a standard A4=440Hz tuning.
      if (cents != SV_TUNING_NOTE_RESET) {
        float hz = sv_tuning_cents_to_hz(cents, base_freq);
        table[i] = hz;

        highest_defined_h = i;
        highest_defined_p = cents;
      }
      else {
        // First notes are not defined, use defaults
        if (highest_defined_h == 0) {
          cents = 100.0 * i;
          note_definitions[i] = cents;
        }
        // Some notes have been defined, use autocomplete.
        else {
          note_definitions[i] =
              note_definitions[i - highest_defined_h] + highest_defined_p;
          cents = note_definitions[i];
        }
        float hz = sv_tuning_cents_to_hz(cents, base_freq);
        table[i] = hz;
      }
    }
  }


  return 0;
}

int sv_tuning_parse_file(float* restrict table, uint table_size,
                         const char* restrict filename) {
  assert(table_size >= 128);
  assert(table);
  assert(filename);

  FILE* f = fopen(filename, "r");
  if (!f) {
    return -1;
  }

  bool is_exact = false;
  float note_definitions[128] = {0};
  float base_freq = 0.0;
  if (sv_tuning_read_file(f, &is_exact, note_definitions, 128, &base_freq)) {
    return -1;
  }
  fclose(f);

  int ret = sv_tuning_calculate_freqs(table, table_size, note_definitions, 128,
                                      is_exact, base_freq);

  return ret;
}
