#ifndef SV_PRINT_H
#define SV_PRINT_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief TODO Doc
 */
void sv_print_char(char c);

/**
 * @brief TODO Doc
 */
void sv_print_str(char* str);

/**
 * @brief TODO Doc
 */
void sv_print_int(int i);

/**
 * @brief TODO Doc
 */
void sv_print_float(float f, int digits);

#ifdef __cplusplus
}
#endif

#endif // SV_PRINT
