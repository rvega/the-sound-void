#ifndef SV_MIDI_H
#define SV_MIDI_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include "basics.h"
#include <stdbool.h>

/////////////////////////////////////////////////
// Midi status bytes

// Channel voice messages
#define SV_MIDI_NOTE_OFF 0x80
#define SV_MIDI_NOTE_ON 0x90
#define SV_MIDI_POLY_AFTERTOUCH 0xA0
#define SV_MIDI_CONTROL_CHANGE 0xB0
#define SV_MIDI_PROGRAM_CHANGE 0XC0
#define SV_MIDI_CHANNEL_AFTERTOUCH 0XD0
#define SV_MIDI_PITCH_BEND 0XE0

// System common messages
#define SV_MIDI_START_SYSEX 0XF0
#define SV_MIDI_TIME_CODE 0XF1
#define SV_MIDI_SONG_POSITION 0XF2
#define SV_MIDI_SONG_SELECT 0XF3
#define SV_MIDI_TUNE_REQUEST 0xF6
#define SV_MIDI_END_SYSEX 0XF7

// System real-time messages
#define SV_MIDI_TIMING_CLOCK 0xF8
#define SV_MIDI_START 0xFA
#define SV_MIDI_CONTINUE 0xFB
#define SV_MIDI_STOP 0xFC
#define SV_MIDI_ACTIVE_SENSING 0xFE
#define SV_MIDI_STATUS_RESET 0xFF

////////////////////////////////////////////////
// Channel mode messages

// These are also called channel mode messages and are the same as control
// change messages with the following controller numbers:
#define SV_MIDI_ALL_SOUND_OFF 120
#define SV_MIDI_RESET_ALL_CONTROLLERS 121
#define SV_MIDI_LOCAL_CONTROL 122
#define SV_MIDI_ALL_NOTES_OFF 123
#define SV_MIDI_OMNI_OFF 124
#define SV_MIDI_OMNI_ON 125
#define SV_MIDI_MONO_ON 126
#define SV_MIDI_POLY_ON 127

#ifdef __cplusplus
}
#endif

#endif /* ifndef SV_MIDI_H */
