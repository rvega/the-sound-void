#ifndef SV_BASICS_H
#define SV_BASICS_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * @brief unsigned integer type
 */
typedef unsigned int uint;

/*
 * @brief unsigned char type
 */
typedef unsigned char uchar;

/*
 * @brief null constant definition
 */
#define null 0

/*
 * @brief TODO Doc
 */
int sv_init(void);

#ifdef __cplusplus
}
#endif

#endif /* ifndef SV_BASICS_H */
