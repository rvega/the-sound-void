#ifndef TUNING_H
#define TUNING_H

#ifdef __cplusplus
extern "C" {
#endif

#include "basics.h"

// TODO Document
// This is a parser for .tun version 1.0 files as described in
// https://www.mark-henning.de/files/am/Tuning_File_V2_Doc.pdf

// Fills the table. Index of the array is MIDI note number (0 to 127) and value is tuning of that
// note in Hz. Returns 0 on success.
// Uses fopen() fclose(), fgets(), strlen(), isspace(), strncmp(), pow()
int sv_tuning_parse_file(float* table, uint table_size, const char* filename);

#ifdef __cplusplus
}
#endif

#endif /* TUNING */
