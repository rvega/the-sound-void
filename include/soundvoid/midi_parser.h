#ifndef SV_MIDI_PARSER_H
#define SV_MIDI_PARSER_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include "basics.h"
#include "midi.h"
#include <stdbool.h>

/** @file */

/**
 * @brief Enumeration of the possible internal states of a sv_midi_parser
 * object. No need to deal with this in application code.
 */
enum sv_midi_parser_state {
  SV_MIDI_STATE_RESET = 0,
  SV_MIDI_STATE_STATUS,
  SV_MIDI_STATE_DATA_1_OF_2,
  SV_MIDI_STATE_DATA_2_OF_2,
  SV_MIDI_STATE_DATA_1_OF_1,
  SV_MIDI_STATE_SYSEX
};
typedef enum sv_midi_parser_state sv_midi_parser_state;

/**
 * @struct sv_midi_parser
 * @brief sv_midi_parser struct holds internal state for subsequent calls of
 * sv_midi_parse(). There is no need to access the struct values directly in
 * application code.
 */
typedef struct sv_midi_parser sv_midi_parser;
struct sv_midi_parser {
  /** @brief Finite state machine */
  sv_midi_parser_state state;

  /** @brief incoming midi bytes are stored here until a message is complete */
  uchar previous_bytes[2];

  /** @brief last status byte that arrived. */
  uchar running_status;

  /** @brief number of data bytes we're expecting. */
  uchar expected_data_bytes;

  /** @brief Current basic midi channel. */
  uint channel;

  /** @brief true if omni mode is active. */
  bool omni;
};

/**
 * @brief Initializes an instance of midi_parser.
 */
void sv_midi_parser_init(sv_midi_parser *x);

/**
 * @brief Read incoming midi bytes and trigger relevant callback functions.
 * @param x pointer to a sv_midi_parser object.
 * @param n number of incoming bytes.
 * @param bytes pointer to the first incoming byte.
 */
void sv_midi_parse(sv_midi_parser *x, uint n, const uchar *bytes,
                   void *user_data);

/**
 * @brief Set omni mode for a midi parser.
 * @param x pointer to a sv_midi_parser object.
 * @param omni_mode When true, parser will respond to messages for all channels.
 *                  When false, parser will only respond to messages for the
 *                  current basic channel.
 */
void sv_midi_set_omni(sv_midi_parser *x, bool omni_mode);

/**
 * @brief Set basic midi channel for a midi parser.
 * @param x pointer to a sv_midi_parser object.
 * @param channel A number between 1 and 16.
 */
void sv_midi_set_channel(sv_midi_parser *x, uint channel);

///////////////////////////////////////////////////////////////////////////////

/* clang-format off */

#ifndef SV_MIDI_CBK_ALL
  /**
   * @brief This function will be called by sv_midi_parse() when any message is
   * read. See @ref SV_MIDI_CBK_ALL
   */
  inline bool sv_midi_cbk_all(uchar status, uchar data1, uchar data2, void* user_data) {
    (void)status;
    (void)data1;
    (void)data2;
    (void)user_data;

    return true;
  }

  /** 
   * @def SV_MIDI_CBK_TIMING_CLOCK 
   * @brief If users need the sv_midi_cbk_timing_clock() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_ALL 1
#else
  bool sv_midi_cbk_all(uchar status, uchar data1, uchar data2, void* user_data);
#endif 

#ifndef SV_MIDI_CBK_NOTE_OFF
  /** 
   * @def SV_MIDI_CBK_NOTE_OFF
   * @brief If users need the sv_midi_cbk_note_off() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_NOTE_OFF 1

  /**
   * @brief This function will be called by sv_midi_parse() when a note off 
   * message is read. See @ref SV_MIDI_CBK_NOTE_OFF .
   */
  inline void sv_midi_cbk_note_off(uchar channel, uchar key, uchar velocity, void* user_data) {
    (void)user_data;
    (void)channel;
    (void)key;
    (void)velocity;
  }
#else
  void sv_midi_cbk_note_off(uchar channel, uchar key, uchar velocity, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_NOTE_ON
  /** 
   * @def SV_MIDI_CBK_NOTE_ON
   * @brief If users need the sv_midi_cbk_note_on() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_NOTE_ON 1

  /**
   * @brief This function will be called by sv_midi_parse() when a note on 
   * message is read. See @ref SV_MIDI_CBK_NOTE_ON .
   */
  inline void sv_midi_cbk_note_on(uchar channel, uchar key, uchar velocity, void* user_data) {
    (void)user_data;
    (void)channel; 
    (void)key; 
    (void)velocity; 
  }
#else
  void sv_midi_cbk_note_on(uchar channel, uchar key, uchar velocity, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_POLY_AFTERTOUCH
  /** 
   * @def SV_MIDI_CBK_POLY_AFTERTOUCH
   * @brief If users need the sv_midi_cbk_aftertouch() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_POLY_AFTERTOUCH 1

  /**
   * @brief This function will be called by sv_midi_parse() when a aftertouch message is
   * read. See @ref SV_MIDI_CBK_POLY_AFTERTOUCH .
   */
  inline void sv_midi_cbk_poly_aftertouch(uchar channel, uchar note, uchar value, void* user_data) {
    (void)user_data;
    (void)channel;
    (void)note;
    (void)value;
  }
#else
  void sv_midi_cbk_poly_aftertouch(uchar channel, uchar note, uchar value, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_CONTROL_CHANGE
  /** 
   * @def SV_MIDI_CBK_CONTROL_CHANGE
   * @brief If users need the sv_midi_cbk_control_change() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_CONTROL_CHANGE 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control change message is
   * read. See @ref SV_MIDI_CBK_CONTROL_CHANGE .
   */
  inline void sv_midi_cbk_control_change(uchar channel, uchar controller, uchar value, void* user_data) {
    (void)user_data;
    (void)channel;
    (void)controller;
    (void)value;
  }
#else
  void sv_midi_cbk_control_change(uchar channel, uchar controller, uchar value, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_PROGRAM_CHANGE
  /** 
   * @def SV_MIDI_CBK_PROGRAM_CHANGE
   * @brief If users need the sv_midi_cbk_program_change() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_PROGRAM_CHANGE 1

  /**
   * @brief This function will be called by sv_midi_parse() when a program 
   * change message is read. See @ref SV_MIDI_CBK_PROGRAM_CHANGE .
   */
  inline void sv_midi_cbk_program_change(uchar channel, uchar program_number, void* user_data) {
    (void)user_data;
    (void)channel;
    (void)program_number;
  }
#else
  void sv_midi_cbk_program_change(uchar channel, uchar program_number, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_CHANNEL_AFTERTOUCH
  /** 
   * @def SV_MIDI_CBK_CHANNEL_AFTERTOUCH
   * @brief If users need the sv_midi_cbk_channel_aftertouch() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_CHANNEL_AFTERTOUCH 1

  /**
   * @brief This function will be called by sv_midi_parse() when a channel aftertouch message is
   * read. See @ref SV_MIDI_CBK_CHANNEL_AFTERTOUCH .
   */
  inline void sv_midi_cbk_channel_aftertouch(uchar channel, uchar value, void* user_data) {
    (void)user_data;
    (void)channel;
    (void)value;
  }
#else
  void sv_midi_cbk_channel_aftertouch(uchar channel, uchar value, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_PITCHBEND
  /** 
   * @def SV_MIDI_CBK_PITCHBEND
   * @brief If users need the sv_midi_cbk_pitchbend() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_PITCHBEND 1

  /**
   * @brief This function will be called by sv_midi_parse() when a pitchbend message is
   * read. See @ref SV_MIDI_CBK_PITCHBEND .
   */
  inline void sv_midi_cbk_pitchbend(uchar channel, uint value, void* user_data) {
    (void)user_data;
    (void)channel;
    (void)value;
  }
#else
  void sv_midi_cbk_pitchbend(uchar channel, uint value, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_ALL_SOUND_OFF
  /** 
   * @def SV_MIDI_ALL_SOUND_OFF
   * @brief If users need the sv_midi_cbk_all_sound_off() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_ALL_SOUND_OFF 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 120 is read. 
   * See @ref SV_MIDI_CBK_ALL_SOUND_OFF .
   */
  inline void sv_midi_cbk_all_sound_off(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_all_sound_off(uchar channel, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_RESET_ALL_CONTROLLERS
  /** 
   * @def SV_MIDI_CBK_RESET_ALL_CONTROLLERS
   * @brief If users need the sv_midi_cbk_reset_all_controllers() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_RESET_ALL_CONTROLLERS 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 121 is read. 
   * See @ref SV_MIDI_CBK_RESET_ALL_CONTROLLERS .
   */
  inline void sv_midi_cbk_reset_all_controllers(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_reset_all_controllers(uchar channel, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_LOCAL_CONTROL
  /** 
   * @def SV_MIDI_LOCAL_CONTROL
   * @brief If users need the sv_midi_cbk_local_control() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_LOCAL_CONTROL 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 122 is read. 
   * See @ref SV_MIDI_CBK_LOCAL_CONTROL .
   */
  inline void sv_midi_cbk_local_control(uchar channel, bool local_control_on, void* user_data) {
    (void)user_data;
    (void)channel; 
    (void)local_control_on;
  }
#else
  void sv_midi_cbk_local_control(uchar channel, bool local_control_on, void* user_data);
#endif

#ifndef SV_MIDI_CBK_ALL_NOTES_OFF
  /** 
   * @def SV_MIDI_ALL_NOTES_OFF
   * @brief If users need the sv_midi_cbk_all_notes_off() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_ALL_NOTES_OFF 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 123 is read. 
   * See @ref SV_MIDI_CBK_ALL_NOTES_OFF .
   */
  inline void sv_midi_cbk_all_notes_off(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_all_notes_off(uchar channel, void* user_data);
#endif


#ifndef SV_MIDI_CBK_OMNI_OFF
  /** 
   * @def SV_MIDI_OMNI_OFF
   * @brief If users need the sv_midi_cbk_omni_off() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_OMNI_OFF 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 124 is read. 
   * See @ref SV_MIDI_CBK_OMNI_OFF .
   */
  inline void sv_midi_cbk_omni_off(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_omni_off(uchar channel, void* user_data);
#endif


#ifndef SV_MIDI_CBK_OMNI_ON
  /** 
   * @def SV_MIDI_OMNI_ON
   * @brief If users need the sv_midi_cbk_omni_on() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_OMNI_ON 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 125 is read. 
   * See @ref SV_MIDI_CBK_OMNI_ON .
   */
  inline void sv_midi_cbk_omni_on(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_omni_on(uchar channel, void* user_data);
#endif


#ifndef SV_MIDI_CBK_MONO_ON
  /** 
   * @def SV_MIDI_MONO_ON
   * @brief If users need the sv_midi_cbk_mono_on() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_MONO_ON 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 126 is read. 
   * See @ref SV_MIDI_CBK_MONO_ON .
   */
  inline void sv_midi_cbk_mono_on(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_mono_on(uchar channel, void* user_data);
#endif


#ifndef SV_MIDI_CBK_POLY_ON
  /** 
   * @def SV_MIDI_POLY_ON
   * @brief If users need the sv_midi_cbk_poly_on() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_POLY_ON 1

  /**
   * @brief This function will be called by sv_midi_parse() when a control 
   * message with controller value equal to 127 is read. 
   * See @ref SV_MIDI_CBK_POLY_ON .
   */
  inline void sv_midi_cbk_poly_on(uchar channel, void* user_data) {
    (void)user_data;
    (void)channel; 
  }
#else
  void sv_midi_cbk_poly_on(uchar channel, void* user_data);
#endif

#ifndef SV_MIDI_CBK_START_SYSEX
  /** 
   * @def SV_MIDI_CBK_START_SYSEX
   * @brief If users need the sv_midi_cbk_sysex() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_START_SYSEX 1

  /**
   * @brief This function will be called by sv_midi_parse() when a start sysex 
   * message is read. See @ref SV_MIDI_CBK_START_SYSEX .
   */
  inline void sv_midi_cbk_start_sysex(uchar byte, void* user_data) {
    (void)user_data;
    (void)byte;
  }
#else
  void sv_midi_cbk_start_sysex(uchar byte, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_SYSEX
  /** 
   * @def SV_MIDI_CBK_SYSEX
   * @brief If users need the sv_midi_cbk_sysex() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_SYSEX 1

  /**
   * @brief This function will be called by sv_midi_parse() when a byte from a 
   * sysex message is read. See @ref SV_MIDI_CBK_SYSEX .
   */
  inline void sv_midi_cbk_sysex(uchar byte, void* user_data) {
    (void)user_data;
    (void)byte;
  }
#else
  void sv_midi_cbk_sysex(uchar byte, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_TIME_CODE
  /** 
   * @def SV_MIDI_CBK_TIME_CODE
   * @brief If users need the sv_midi_cbk_time_code() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_TIME_CODE 1

  /**
   * @brief This function will be called by sv_midi_parse() when a time code message is
   * read. See @ref SV_MIDI_CBK_TIME_CODE .
   */
  inline void sv_midi_cbk_time_code(uchar message_type, uchar value, void* user_data) {
    (void)user_data;
    (void)message_type;
    (void)value;
  }
#else
  void sv_midi_cbk_time_code(uchar message_type, uchar value, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_SONG_POSITION
  /** 
   * @def SV_MIDI_CBK_SONG_POSITION
   * @brief If users need the sv_midi_cbk_song_position() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_SONG_POSITION 1

  /**
   * @brief This function will be called by sv_midi_parse() when a song position message is
   * read. See @ref SV_MIDI_CBK_SONG_POSITION .
   */
  inline void sv_midi_cbk_song_position(uchar value, void* user_data) {
    (void)user_data;
    (void)value;
  }
#else
  void sv_midi_cbk_song_position(uchar value, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_SONG_SELECT
  /** 
   * @def SV_MIDI_CBK_SONG_SELECT
   * @brief If users need the sv_midi_cbk_song_select() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_SONG_SELECT 1

  /**
   * @brief This function will be called by sv_midi_parse() when a song select message is
   * read. See @ref SV_MIDI_CBK_SONG_SELECT .
   */
  inline void sv_midi_cbk_song_select(uchar song, void* user_data) {
    (void)user_data;
    (void)song;
  }
#else
  void sv_midi_cbk_song_select(uchar song, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_TUNE_REQUEST
  /** 
   * @def SV_MIDI_CBK_TUNE_REQUEST
   * @brief If users need the sv_midi_cbk_tune_request() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_TUNE_REQUEST 1

  /**
   * @brief This function will be called by sv_midi_parse() when a tune request message is
   * read. See @ref SV_MIDI_CBK_TUNE_REQUEST .
   */
  inline void sv_midi_cbk_tune_request(void* user_data) {
    (void)user_data;
  }
#else
  void sv_midi_cbk_tune_request(void* user_data);
#endif 


#ifndef SV_MIDI_CBK_END_SYSEX
  /** 
   * @def SV_MIDI_CBK_END_SYSEX
   * @brief If users need the sv_midi_cbk_end_sysex() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_END_SYSEX 1

  /**
   * @brief This function will be called by sv_midi_parse() when a end sysex 
   * message is received sysex message is read. See @ref SV_MIDI_CBK_END_SYSEX .
   */
  inline void sv_midi_cbk_end_sysex(uchar byte, void* user_data) {
    (void)user_data;
    (void)byte;
  }
#else
  void sv_midi_cbk_end_sysex(uchar byte, void* user_data);
#endif 


#ifndef SV_MIDI_CBK_TIMING_CLOCK
  /**
   * @brief This function will be called by sv_midi_parse() when a timing clock message is
   * read. See @ref SV_MIDI_CBK_TIMING_CLOCK.
   */
  inline void sv_midi_cbk_timing_clock(void* user_data) {
    (void)user_data;
  }

  /** 
   * @def SV_MIDI_CBK_TIMING_CLOCK 
   * @brief If users need the sv_midi_cbk_timing_clock() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_TIMING_CLOCK 1
#else
  void sv_midi_cbk_timing_clock(void* user_data);
#endif 


#ifndef SV_MIDI_CBK_START
  /**
   * @brief This function will be called by sv_midi_parse() when a start message is
   * read. See @ref SV_MIDI_CBK_START .
   */
  inline void sv_midi_cbk_start(void* user_data) {
    (void)user_data;
  }

  /** 
   * @def SV_MIDI_CBK_START
   * @brief If users need the sv_midi_cbk_start() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_START 1
#else
  void sv_midi_cbk_start(void* user_data);
#endif 


#ifndef SV_MIDI_CBK_CONTINUE
  /** 
   * @def SV_MIDI_CBK_CONTINUE
   * @brief If users need the sv_midi_cbk_continue() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_CONTINUE 1

  /**
   * @brief This function will be called by sv_midi_parse() when a continue message is
   * read. See @ref SV_MIDI_CBK_CONTINUE .
   */
  inline void sv_midi_cbk_continue(void* user_data) {
    (void)user_data;
  }
#else
  void sv_midi_cbk_continue(void* user_data);
#endif 
  

#ifndef SV_MIDI_CBK_STOP
  /** 
   * @def SV_MIDI_CBK_STOP
   * @brief If users need the sv_midi_cbk_stop() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_STOP 1

  /**
   * @brief This function will be called by sv_midi_parse() when a stop message is
   * read. See @ref SV_MIDI_CBK_STOP .
   */
  inline void sv_midi_cbk_stop(void* user_data) {
    (void)user_data;
  }
#else
  void sv_midi_cbk_stop(void* user_data);
#endif 


#ifndef SV_MIDI_CBK_ACTIVE_SENSING
  /** 
   * @def SV_MIDI_CBK_ACTIVE_SENSING
   * @brief If users need the sv_midi_cbk_active_sensing() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_ACTIVE_SENSING 1

  /**
   * @brief This function will be called by sv_midi_parse() when a active sensing message is
   * read. See @ref SV_MIDI_CBK_ACTIVE_SENSING .
   */
  inline void sv_midi_cbk_active_sensing(void* user_data) {
    (void)user_data;
  }
#else
  void sv_midi_cbk_active_sensing(void* user_data);
#endif 


#ifndef SV_MIDI_CBK_RESET
  /** 
   * @def SV_MIDI_CBK_RESET
   * @brief If users need the sv_midi_cbk_reset() callback, they must 
   * define this constant in the compiler command line and define the callback 
   * function in any compilation unit. When this constant is not defined, an 
   * empty implementation of the callback function will be defined.
   */
  #define SV_MIDI_CBK_RESET 1

  /**
   * @brief This function will be called by sv_midi_parse() when a reset message 
   * is read. See @ref SV_MIDI_CBK_RESET .
   */
  inline void sv_midi_cbk_reset(void* user_data) {
    (void)user_data;
  }
#else
  void sv_midi_cbk_reset(void* user_data);
#endif 


















/* clang-format on */

#ifdef __cplusplus
}
#endif

#endif /* ifndef SV_MIDI_PARSER_H */
