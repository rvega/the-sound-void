#ifndef POLY_H
#define POLY_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "basics.h"

// The poly object receives a stream of note-on and note-off events and dispatches note-on and
// note-off callbacks to the different synth voices, depending on the number of voices that is
// selected. When the number of available voices is exhausted and a new note-on arrives, the older
// note is turned off. Also, when the number of voices is 1, there is a special "mono mode" that
// triggers a note-on when a note is released and there are pressed notes in the keyboard.
// Monosynths are nice.
//
// This is similar to the way the Arturia synths work and also how the Prophet 8 rev 2 works when
// the key mode is set to "Last R" (I think the R stands for release (?)). It would be nice to have
// a setting like the prophet synths for choosing between Hi, Low, Last, Hi R, Low R and Last R
// modes. This would probably require an extra callback sv_poly_cbk_pitch() that would allow the
// poly object to set the pitch of a voice without triggering the release or the attack stages of
// the envelope. (That's how the non-R modes work in the Prophet).
//
// How does Moog do this?
//


#ifndef SV_POLY_MAX_VOICES
// Max allocated slots for voices (How many simultaneous voices we can hold. The actual number of
// voices is set dynamically. This is for allocation purposes).
#define SV_POLY_MAX_VOICES 16
#endif

#ifndef SV_POLY_MAX_NOTES
// Max allocated slots for active notes (How many previously pressed notes we can remember).
#define SV_POLY_MAX_NOTES 16
#endif

struct sv_poly_node {
  uint order;
  uchar note;
  bool active;
};
typedef struct sv_poly_node sv_poly_node;

struct sv_poly {
  uint counter_notes;
  uint counter_voices;

  uint num_voices;
  uint active_voices;
  sv_poly_node voices[SV_POLY_MAX_VOICES];

  uint active_notes;
  sv_poly_node notes[SV_POLY_MAX_NOTES];

  void* user_data;
};
typedef struct sv_poly sv_poly;

void sv_poly_init(sv_poly* x);
void sv_poly_reset(sv_poly* x);
void sv_poly_set_num_voices(sv_poly* x, uint n);
void sv_poly_set_user_data(sv_poly* restrict x, void* restrict user_data);
void sv_poly_note_on(sv_poly* restrict x, uchar note, uchar velocity);
void sv_poly_note_off(sv_poly* restrict x, uchar note, uchar velocity);


#ifndef SV_POLY_CBK_NOTE_OFF
/**
 * @def SV_POLY_CBK_NOTE_OFF
 * @brief If users need the sv_poly_cbk_note_off() callback, they must
 * define this constant in the compiler command line and define the callback
 * function in any compilation unit. When this constant is not defined, an
 * empty implementation of the callback function will be defined.
 */
#define SV_POLY_CBK_NOTE_OFF 1

/**
 * @brief This function will be called by sv_poly_note_on() and sv_poly_cbk_note_off().
 * See @ref SV_POLY_CBK_NOTE_OFF .
 */
inline void sv_poly_cbk_note_off(uint voice, uchar note, uchar velocity, void* user_data) {
  (void)user_data;
  (void)voice;
  (void)note;
  (void)velocity;
}
#else
void sv_poly_cbk_note_off(uint voice, uchar note, uchar velocity, void* user_data);
#endif


#ifndef SV_POLY_CBK_NOTE_ON
/**
 * @def SV_POLY_CBK_NOTE_ON
 * @brief If users need the sv_poly_cbk_note_on() callback, they must
 * define this constant in the compiler command line and define the callback
 * function in any compilation unit. When this constant is not defined, an
 * empty implementation of the callback function will be defined.
 */
#define SV_POLY_CBK_NOTE_ON 1

/**
 * @brief This function will be called by sv_poly_note_on() and sv_poly_cbk_note_on().
 * See @ref SV_POLY_CBK_NOTE_ON .
 */
inline void sv_poly_cbk_note_on(uint voice, uchar note, uchar velocity, void* user_data) {
  (void)user_data;
  (void)voice;
  (void)note;
  (void)velocity;
}
#else
void sv_poly_cbk_note_on(uint voice, uchar note, uchar velocity, void* user_data);
#endif


#ifdef __cplusplus
}
#endif


#endif /* POLY */
