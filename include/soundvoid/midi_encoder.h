#ifndef SV_MIDI_ENCODER_H
#define SV_MIDI_ENCODER_H 1

#ifdef __cplusplus
extern "C" {
#endif

#include "basics.h"
#include "midi.h"
#include <stdbool.h>

/** @file */

/* TODO:
 *      * document
 *      * only send running status for mode and control change messages.
 *      * Haven't really looked at midi time code.
 *      * Haven't really looked at universal exclusive messages.
 *      * If sending a lot of data with running status, re-send the status byte
 *        "every few seconds" (maybe a few hundred messages?).
 *      * CC from 96 through 101 are used differently, as increment/decrement
 *        and parameter number. We're not supporting that.
 *      */

struct sv_midi_encoder {
  uchar previous_status_byte;
  uint running_status_counter;
};
typedef struct sv_midi_encoder sv_midi_encoder;

void sv_midi_encoder_init(sv_midi_encoder* x);


uint sv_midi_enc_note_off(sv_midi_encoder* x, uchar channel, uchar note, uchar velocity,
                          uchar* bytes);
uint sv_midi_enc_note_on(sv_midi_encoder* x, uchar channel, uchar note, uchar velocity,
                         uchar* bytes);

uint sv_midi_enc_poly_aftertouch(sv_midi_encoder* x, uchar channel, uchar note, uchar value,
                                 uchar* bytes);

uint sv_midi_enc_control_change(sv_midi_encoder* x, uchar chan, uchar controller, uint value,
                                uchar* bytes);

uint sv_midi_enc_control_change_precise(sv_midi_encoder* x, uchar chan, uchar controller,
                                        uint value, uchar* bytes);


uint sv_midi_enc_program_change(sv_midi_encoder* x, uchar channel, uchar program, uchar* bytes);
uint sv_midi_enc_channel_aftertouch(sv_midi_encoder* x, uchar channel, uchar value, uchar* bytes);
uint sv_midi_enc_pitchbend(sv_midi_encoder* x, uchar channel, int value, uchar* bytes);

uint sv_midi_enc_all_sound_off(sv_midi_encoder* x, uchar channel, uchar* bytes);
uint sv_midi_enc_reset_all_ctls(sv_midi_encoder* x, uchar channel, uchar* bytes);
uint sv_midi_enc_local_ctl(sv_midi_encoder* x, uchar channel, bool local_control_on, uchar* bytes);
uint sv_midi_enc_all_notes_off(sv_midi_encoder* x, uchar channel, uchar* bytes);
uint sv_midi_enc_omni_off(sv_midi_encoder* x, uchar channel, uchar* bytes);
uint sv_midi_enc_omni_on(sv_midi_encoder* x, uchar channel, uchar* bytes);
uint sv_midi_enc_mono_on(sv_midi_encoder* x, uchar channel, uchar num_channels, uchar* bytes);
uint sv_midi_enc_poly_on(sv_midi_encoder* x, uchar channel, uchar* bytes);

uint sv_midi_enc_start_sysex(uchar* bytes);
uint sv_midi_enc_time_code(uchar message_type, uchar value, uchar* bytes);
uint sv_midi_enc_song_position(uint value, uchar* bytes);
uint sv_midi_enc_song_select(uchar song, uchar* bytes);
uint sv_midi_enc_tune_request(uchar* bytes);
uint sv_midi_enc_end_sysex(uchar* bytes);

uint sv_midi_enc_timing_clock(uchar* bytes);
uint sv_midi_enc_continue(uchar* bytes);
uint sv_midi_enc_start(uchar* bytes);
uint sv_midi_enc_stop(uchar* bytes);
uint sv_midi_enc_active_sensing(uchar* bytes);
uint sv_midi_enc_reset(uchar* bytes);

#ifdef __cplusplus
}
#endif

#endif /* ifndef SV_MIDI_ENCODER_H */
