#ifndef SV_TIME_H
#define SV_TIME_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief TODO Doc
 */
struct sv_timespec {
  unsigned long tv_sec;
  unsigned long tv_nsec;
};
typedef struct sv_timespec sv_timespec;

/**
 * @brief TODO Doc
 */
int sv_timespec_get(sv_timespec* ts);

/**
 * @brief TODO Doc
 */
float sv_timespec_diff(sv_timespec t1, sv_timespec t2);

#ifdef __cplusplus
}
#endif

#endif // SV_TIME
