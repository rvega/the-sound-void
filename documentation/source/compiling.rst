**********
Compiling.
**********

Install these tools in your system:

* `Ceedling <https://github.com/ThrowTheSwitch/Ceedling>`_
* CMake
* Ninja
* GCC

Compile the first time:

.. code-block:: bash

  mkdir build
  cd build 
  cmake -G Ninja -DCMAKE_BUILD_TYPE=debug ..

Compile following times:

.. code-block:: bash

  cd build 
  ninja # compila la libreria
  ninja run_tests # compila y corre los tests con ceedling
