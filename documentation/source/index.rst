.. include:: ../../README.rst


.. toctree::
   :maxdepth: 1
   :caption: Documentation:
   :hidden:

   intro
   compiling
   tests

.. toctree::
   :maxdepth: 5
   :caption: API:
   :hidden:

   Full API. <api/library_root.rst>
   Alphabetical Index. <genindex>
   midi
   basics
